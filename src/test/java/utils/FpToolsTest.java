/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.FpTools.Function;

/**
 *
 * @author rudolph
 */
public class FpToolsTest {
    
    public FpToolsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of map method, of class FpTools.
     */
    @Test
    public void testMap() {
        System.out.println("map");
        
        Collection<Integer> source = new ArrayList<Integer>();
        for(int i = 0; i < 10; i++ ){                     
            source.add(i);
        }
        Collection<String> result = FpTools.map(new Function<Integer,String>(){
                                                    @Override
                                                    public String invoke(Integer i){
                                                        return i.toString();
                                                    }    
                                                }, source);
        // inverse function
        Collection<Integer> result2 = FpTools.map(new Function<String, Integer>(){
                                                    @Override
                                                    public Integer invoke(String s){
                                                        return Integer.parseInt(s);
                                                    }
                                                  }, result );
        
                                            
    
        assertEquals(source, result2);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
}