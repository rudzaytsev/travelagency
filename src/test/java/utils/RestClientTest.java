/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import com.mycompany.travelagency.repository.Criteria;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author rudolph
 */
public class RestClientTest {
    
    public RestClientTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    

    /**
     * Test of postRequest method, of class RestClient.
     */
    @Ignore
    @Test
    public void testPostRequest() {
        System.out.println("postRequest");
        
        String url = "http://testbooking.salesplatform.ru:8080/TourBooking/webresources/service";
        /*
        JSONObject structure = new JSONObject();
        structure.put("messageType", "FindTours");
        structure.put("requesterId", 1);
        structure.put("offset", 0);
        structure.put("limit", 1000);
        
        JSONArray arr = new JSONArray();
        JSONObject params = new JSONObject();
        params.put("name", "country");
        params.put("value", "Россия");
        params.put("condition", "eq");
        arr.add(params);
        
        structure.put("findParams", arr);
        */
        
        // new code
        
          Criteria instance = new Criteria();
          instance.addCriteria("messageType", "FindTours");
          instance.addCriteria("requesterId", 1l);
          instance.addCriteria("offset", 0l);
          instance.addCriteria("limit", 1000l);
          
          Criteria cparams = new Criteria();
          cparams.addCriteria("name", "country");
          cparams.addCriteria("value", "Россия");
          cparams.addCriteria("condition", "eq");
          JSONArray cArray = new JSONArray();
          cArray.add(cparams.toJsonObject());
          
          instance.addCriteriaArray("findParams", cArray);
        
        
        
        
        // end new code
        
        RestClient client = new RestClient();
        client.setUser("1");
        client.setPassword("1");
        client.setUrl(url);
        RestClient.RequestParams reqParams = client.new RequestParams();
        reqParams.setContentType("application/json");
        reqParams.setVersion("2");
        //reqParams.setRequestBody( structure.toString());
        
        reqParams.setRequestBody(instance.toString());
        
        client.setRequestParams(reqParams);
        String content = client.postRequest();
        
        System.out.println(client.getResponseData().getReason());
        System.out.println(client.getResponseData().getReason());
        
        assertNotNull(content);
        assertEquals(200, client.getResponseData().getStatusCode());
        
        
        
    }
}