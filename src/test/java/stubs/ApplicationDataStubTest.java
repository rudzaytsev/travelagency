/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package stubs;

import com.mycompany.travelagency.logic.Company;
import com.mycompany.travelagency.logic.TourOperator;
import com.mycompany.travelagency.logic.TravelAgency;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author rudolph
 */
public class ApplicationDataStubTest {
    
    public ApplicationDataStubTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class ApplicationDataStub.
     */
    @Ignore
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        ApplicationDataStub firstInstance = ApplicationDataStub.getInstance();
        TravelAgency agency = new TravelAgency();
        agency.setName("MyTestAgency");
        firstInstance.addCompanies( agency);
        ApplicationDataStub sameInstance = ApplicationDataStub.getInstance();
        
        assertEquals( firstInstance, sameInstance );
        
    }

    
    /**
     * Test of setCompanies method, of class ApplicationDataStub.
     */
    @Ignore
    @Test
    public void testAddCompanies() {
        System.out.println("setCompanies");
        Company newCompany = new TravelAgency();
        ApplicationDataStub instance = ApplicationDataStub.getInstance();
        newCompany.setName("TestCompany");
        
        int sizeInit = instance.getAgencies().size();
        
        instance.addCompanies(newCompany);
        
                
        assertTrue(instance.getCompanies().contains(newCompany) );
        assertTrue(instance.getAgencies().contains( (TravelAgency) newCompany));
        assertEquals(sizeInit + 1, instance.getAgencies().size());
        
        
        Company newCompany2 = new TourOperator();
        //newCompany2.setName("TestCompany2");
        
        int sizeBeforeAdd = instance.getOperators().size();
        
        instance.addCompanies(newCompany2);
                
        assertTrue(instance.getCompanies().contains(newCompany2) );
        assertTrue(instance.getOperators().contains((TourOperator) newCompany2));

        assertEquals(sizeBeforeAdd + 1, instance.getOperators().size());
        
    }
    
    @Ignore
    @Test
    public void testSearchOperatorByName(){
        
        System.out.println("searchOperatorByName");
        Company newCompany = new TourOperator();
        
        String testName = "MyTestTourOperator";
        newCompany.setName(testName);
        
        ApplicationDataStub instance = ApplicationDataStub.getInstance();
        
        for( int i = 0; i < 10; i++ ){
            Company cmp = new TourOperator();
            cmp.setName("Tour operator " + i);
            instance.addCompanies(cmp);
            
            // companies without names (special case)
            instance.addCompanies(new TourOperator());
        }
        
        instance.addCompanies(newCompany);
        
        TourOperator result = instance.searchOperatorByName(testName);
        assertEquals(testName,result.getName());
        
        String notInArrayName = "!@#$%^&*()null";
        TourOperator result2 = instance.searchOperatorByName(notInArrayName);
        assertNull(result2);                
        
    } 
    
    
}