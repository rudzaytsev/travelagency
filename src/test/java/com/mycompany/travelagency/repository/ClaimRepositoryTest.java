package com.mycompany.travelagency.repository;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.travelagency.logic.Claim;
import com.mycompany.travelagency.logic.ClaimDirection;
import com.mycompany.travelagency.logic.Customer;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import utils.Utils;

/**
 *
 * @author rudolph
 */
public class ClaimRepositoryTest {
    
    public ClaimRepositoryTest() {      
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Ignore
    @Test
    public void testFindClaimsByDirection() {
    
        System.out.println("testFindClaimsByDirection");
       
        
        ClaimRepository repository = new ClaimRepository();
        long agencyId = 1;
        List<Claim> list = null;
        list = repository.findClaimsByDirection(ClaimDirection.FROM_AGENCY,agencyId );
    
        assertFalse( list.isEmpty());
        assertNotNull(list);
        
        for(Claim c : list){
            System.out.println("Claim id = " + c.getId() );
            System.out.println("Requester = " + c.getRequesterName() );
            System.out.println("Tour Owner = " + c.getTourOwnerName() );
            
            for(Customer customer : c.getCustomers()){
                System.out.println("Customer ruName = " + customer.getRuName());
                System.out.println("Customer sex = " + customer.getSex());
                System.out.println("Customer birthDate = " + customer.getBirthDate());
                
            }
        }
         
    }
}