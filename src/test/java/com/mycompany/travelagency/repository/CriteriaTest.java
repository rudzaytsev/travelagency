/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.repository;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author rudolph
 */
public class CriteriaTest {
    
    public CriteriaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
   

    /**
     * Test of toString method, of class Criteria.
     */
    @Ignore
    @Test
    public void testToString() {
        System.out.println("toString");
        Criteria instance = new Criteria();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
    @Test
    public void testMakeComplexCriteria(){
        
        System.out.println("makeComplexCriteria");
        
        Criteria instance = new Criteria();
        instance.addCriteria("messageType", "FindCompany");
        instance.addCriteria("requesterId", 1l);
        JSONArray arr = new JSONArray();
        arr.add(0);
        arr.add(1);
        instance.addCriteriaArray("types", arr );
        
        
        System.out.println(instance.toString());
    }
    
    @Test
    public void testMakeComplexCriteria2(){
        
          System.out.println("makeComplexCriteria 2");
          
          Criteria instance = new Criteria();
          instance.addCriteria("messageType", "FindTours");
          instance.addCriteria("requesterId", 1l);
          instance.addCriteria("offset", 0l);
          instance.addCriteria("limit", 1000l);
          
          Criteria cparams = new Criteria();
          cparams.addCriteria("name", "country");
          cparams.addCriteria("value", "Россия");
          cparams.addCriteria("condition", "eq");
          JSONArray cArray = new JSONArray();
          cArray.add(cparams.toJsonObject());
          
          instance.addCriteriaArray("findParams", cArray);
          
          System.out.println("INSTANCE -------------------------------");
          System.out.println(instance.toString());
          System.out.println("----------------------------------------");

          
          
          JSONObject structure = new JSONObject();
          structure.put("messageType", "FindTours");
          structure.put("requesterId", 1);
          structure.put("offset", 0);
          structure.put("limit", 1000);
        
          JSONArray arr = new JSONArray();
          JSONObject params = new JSONObject();
          params.put("name", "country");
          params.put("value", "Россия");
          params.put("condition", "eq");
          arr.add(params);
        
          structure.put("findParams", arr);
          
          System.out.println("JSON STUCTURE -------------------------------");
          System.out.println(structure.toString());
          System.out.println("----------------------------------------");

          
          assertEquals(structure.toString()
                  , instance.toString());
          
          
          //assertEquals(new JSONObject(), (new Criteria()).toJsonObject());
                   
                 
        
    } 
    
    
    
    
    
    
    
}