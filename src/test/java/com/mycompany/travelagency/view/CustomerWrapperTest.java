/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.view;

import com.mycompany.travelagency.logic.Customer;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import view.CustomerAdapter;
import view.CustomersWrapper;

/**
 *
 * @author rudolph
 */
public class CustomerWrapperTest {
    
    public CustomerWrapperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void getValidCustomersTest() {
        
        System.out.println("getValidCustomersTest");
    
        CustomersWrapper wrapper = new CustomersWrapper();
      
        CustomerAdapter ca = new CustomerAdapter();
        ca.setLastName("Volkov");
        ca.setFirstName("Sergey");
        ca.setFatherName("Ivanovich");
        ca.setBirthDate("2004-01-01");
        
        ca.setTransborderLastName("Volkov");
        ca.setTransborderFirstName("Sergey");
        ca.setTransborderFatherName("Ivanovich");
                
        ca.setTransborderPassport("9999999");
        ca.setTransborderFromDate("2016-01-01");
        ca.setTransborderToDate("2017-01-02");
        ca.setSex("Male");
        ca.setPhone("89217777777");
        
         
        wrapper.getCustomer2().setLastName("Volkov");
        wrapper.getCustomer2().setFirstName("Sergey");
        wrapper.getCustomer2().setFatherName("Ivanovich");
        wrapper.getCustomer2().setBirthDate("2004-01-01");
        
        wrapper.getCustomer2().setTransborderLastName("Volkov");
        wrapper.getCustomer2().setTransborderFirstName("Sergey");
        wrapper.getCustomer2().setTransborderFatherName("Ivanovich");
                
        wrapper.getCustomer2().setTransborderPassport("9999999");
        wrapper.getCustomer2().setTransborderFromDate("2016-01-01");
        wrapper.getCustomer2().setTransborderToDate("2017-01-02");
        wrapper.getCustomer2().setSex("Male");
        wrapper.getCustomer2().setPhone("89258888888");
        
        
        wrapper.setCustomer1(ca);
        
        List<Customer> result = new ArrayList<Customer>();
        
        List<CustomerAdapter> adapts = wrapper.getCustomersAsList();
        for(CustomerAdapter adapter : adapts){
            Customer customer = adapter.getAdaptedCustomer();
            if(customer.isValid()){
                System.out.println("Valid!");
                result.add(customer);
            }            
        }
        
        assertTrue( result.size() == 2);
        
    }
}