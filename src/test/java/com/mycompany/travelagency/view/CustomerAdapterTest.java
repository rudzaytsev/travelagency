/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.view;

import com.mycompany.travelagency.logic.Customer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import view.CustomerAdapter;

/**
 *
 * @author rudolph
 */
public class CustomerAdapterTest {
    
    public CustomerAdapterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void nameAdaptationTest() {
    
        Customer customer = new Customer();
        customer.setSex("Male");
        customer.setBirthDate("1995-01-21");
                
        CustomerAdapter custAdapt = new CustomerAdapter(customer);
        custAdapt.setLastName("Ivanov");
        custAdapt.setFirstName("Ivan");
        custAdapt.setFatherName("Ivanovich");
        
        Customer adaptedCustomer = custAdapt.getAdaptedCustomer();
        assertEquals("Ivanov Ivan Ivanovich",adaptedCustomer.getRuName());
        
        custAdapt.setTransborderLastName("Ivanov");
        custAdapt.setTransborderFirstName("Ivan");
        custAdapt.setTransborderFatherName("Ivanovich");
        
        Customer nextAdaptedCustomer = custAdapt.getAdaptedCustomer();
        assertEquals("Ivanov Ivan Ivanovich",nextAdaptedCustomer.getTransborderName());
        
    }
}