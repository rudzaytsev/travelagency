/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.logic;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rudolph
 */
public class ClaimTest {
    
    public ClaimTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   

    /**
     * Test of getShortCustomersInfo method, of class Claim.
     */
    @Test
    public void testGetShortCustomersInfo() {
        System.out.println("getShortCustomersInfo");
        Claim instance = new Claim();
        Customer customer = new Customer();
        customer.setTransborderName("Mamontov Petr Ivanovich");
        instance.addCustomer(customer);
                
        String expResult = "Mamontov P.I.";
        
        String result = instance.getShortCustomersInfo();
        System.out.println("Result = "+ result);
        assertEquals(expResult, result);
        
        
        Customer customer2 = new Customer();
        customer2.setTransborderName("Mamontova Ludmila Petrovna");
        instance.addCustomer(customer2);
         
        String expResult2 = "Mamontov P.I., Mamontova L.P.";
        
        String result2 = instance.getShortCustomersInfo();
        System.out.println("Result = "+ result2);
        assertEquals(expResult2, result2);
        
        Customer customer3 = new Customer();
        customer3.setTransborderName("Mamontov Sergey Petrovich");
        instance.addCustomer(customer3);
        
        String expResult3 = "Mamontov P.I., Mamontova L.P., Mamontov S.P.";
        
        String result3 = instance.getShortCustomersInfo();
        System.out.println("Result = "+ result3);
        assertEquals(expResult3, result3);
        
                        
    }
    
    @Test
    public void getShortCustomersInfoTest2() {
    
        System.out.println("getShortCustomersInfoTest2");
        Customer cust = new Customer();
        cust.setRuName("Иванов Иван Иванович");
        cust.setTransborderName("");
        Claim claim = new Claim();
        claim.addCustomer(cust);
        
        String info = claim.getShortCustomersInfo();
        System.out.println(info);
        assertEquals("Иванов И.И.", info);
    
    }
    
    @Test
    public void getShortCustomersInfoTest3() {
    
        System.out.println("getShortCustomersInfoTest3");
        Customer cust = new Customer();
        cust.setRuName("Соколов Антон Игоревич");
        cust.setTransborderName("Anton");
        Claim claim = new Claim();
        claim.addCustomer(cust);
        
        String info = claim.getShortCustomersInfo();
        System.out.println(info);
        assertEquals("Соколов А.И.", info);
    
    }
    
    
}