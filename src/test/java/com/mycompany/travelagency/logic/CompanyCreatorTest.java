/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.logic;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rudolph
 */
public class CompanyCreatorTest {
    
    public CompanyCreatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   
    /**
     * Test of createCompany method, of class CompanyCreator.
     */
    @Test
    public void testCreateCompany() {
        
        System.out.println("createCompany");
        CompanyCreator instance = new CompanyCreator();
        
        instance.setSelectedCompanyType(CompanyCreator.AGENCY);
        String test01 = "Test01";
        instance.setSelectedCompanyName(test01);
        Company result = instance.createCompany();
        
        assertEquals(result.getType(), CompanyCreator.AGENCY);
        assertEquals(test01, result.getName());
        
        instance.setSelectedCompanyType(CompanyCreator.OPERATOR);
        String test02 = "Test02";
        instance.setSelectedCompanyName(test02);
        Company result2 = instance.createCompany();
        
        assertEquals(result2.getType(), CompanyCreator.OPERATOR);
        assertEquals(test02, result2.getName());
        
        instance.setSelectedCompanyType("ANOTHER NOT SUPPORTED TYPE");
        Company result3 = instance.createCompany();
        
        assertNull(result3);

        
    }
}