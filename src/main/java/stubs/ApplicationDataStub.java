/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package stubs;

import com.mycompany.travelagency.logic.Claim;
import com.mycompany.travelagency.logic.ClaimType;
import com.mycompany.travelagency.logic.Company;
import com.mycompany.travelagency.logic.Customer;
import com.mycompany.travelagency.logic.Tour;
import com.mycompany.travelagency.logic.TourOperator;
import com.mycompany.travelagency.logic.TravelAgency;
import java.util.ArrayList;

/**
 *
 * @author rudolph
 */
public class ApplicationDataStub{

    private static ArrayList<Company> companies;
    
    private static ArrayList<TourOperator> operators;
    
    private static ArrayList<TravelAgency> agencies;
    
    private static ArrayList<Claim> claims;
    
    private static ArrayList<Customer> customers;
    
    private static ArrayList<Tour> tours;

    private static final int ITERATIONS = 15;
    
    public  static final int INITIAL_SIZE = 30;
    
    private static void initCustomers(){
        
        
        Customer someCustomer = new Customer();
        someCustomer.setBirthDate("1989-04-13");
        someCustomer.setRuName("Мамонтов Петр Иванович");
        someCustomer.setSex("Male");
        someCustomer.setTransborderName("Mamontov Petr Ivanovich");
        someCustomer.setTransborderPassport("1457 985376");
        someCustomer.setTransborderFromDate("2012-01-01");
        someCustomer.setTransborderToDate("2018-01-01");
        
        customers.add(someCustomer);
        
        Customer otherCustomer = new Customer();
        otherCustomer.setBirthDate("1991-02-17");
        otherCustomer.setRuName("Мамонтова Людмила Петровна");
        otherCustomer.setSex("Female");
        otherCustomer.setTransborderName("Mamontova Ludmila Petrovna");
        otherCustomer.setTransborderPassport("8754 983467");
        otherCustomer.setTransborderFromDate("2013-02-01");
        otherCustomer.setTransborderToDate("2019-03-01");
        
        customers.add(otherCustomer);        
        
    }
    
    private static void initTours(){
        
        for(int i = 0; i < ITERATIONS; i++){
            
            Tour sampleTour = new Tour();
            sampleTour.setLastModified("2014-03-01 18:13:56");
            sampleTour.setId(12400l + i);
            sampleTour.setTourOperatorTourId(5000 + i);
            sampleTour.setOptionalAttribute("country", "Russia");
            
            if( i % 2 == 0){
                sampleTour.setOptionalAttribute("name", "Olympic Sochi");
                sampleTour.setOptionalAttribute("town","Sochi");
            }
            else {
                sampleTour.setOptionalAttribute("name", "Krim Pohod");
                sampleTour.setOptionalAttribute("town","Krim");
            }
            sampleTour.setOptionalAttribute("beginDate", "2014-02-01");
            sampleTour.setOptionalAttribute("endDate", "2014-02-08");
            
            /*
            if( i % 2 == 0){
                sampleTour.getParrentTourId();
            }
            */ 
            
            tours.add(sampleTour);
        }
        
        
    }
    
    
    private ApplicationDataStub(){
        
        companies = new ArrayList();
        operators = new ArrayList();
        agencies = new ArrayList();
        claims = new ArrayList();
        customers = new ArrayList();
        tours = new ArrayList();
        
        for( int i = 0; i < ITERATIONS; i++ ){
                    

            TourOperator to = new TourOperator();
            to.setId(1000l + i);
            to.setName("Neva " + i);
           // to.addSubscription(ta.getName());
            companies.add(to);
            operators.add(to);
            
            TravelAgency ta = new TravelAgency();
            ta.setId((long)i);
            ta.setName("RTA " + i);
            ta.addAttendantOperator(to);
            companies.add(ta);
            agencies.add(ta);
        }
        
        ApplicationDataStub.initCustomers();
        ApplicationDataStub.initTours();
        
        int k = 0;
        for( TourOperator oper : operators ){
            
            String operatorName = oper.getName();
            for( String subAgencyName : oper.getSubscriptedCompanies() ){
                Claim someClaim = new Claim();
                someClaim.setId(2000l + k);
                someClaim.setRequesterName(subAgencyName);
                someClaim.setRequestDate("2014-01-01");
                someClaim.setTourName(tours.get(k).getOptionalAttribute("name"));
                someClaim.setTourId(tours.get(k).getId());
                someClaim.setTourOwnerName(operatorName);
                someClaim.setDuration(10);
                someClaim.setCustomers(customers);
                claims.add(someClaim);
                k++;
            }
            
        }
             
    }
    
    private static class StubHolder{
        
       public static ApplicationDataStub stub = new ApplicationDataStub();
    }
    
    public static ApplicationDataStub getInstance(){
        return StubHolder.stub;
    }
    
    public ArrayList<Company> getCompanies(){
        return companies;
    }
    
    public void addCompanies(Company newCompany){
        companies.add(newCompany);
        if(newCompany.getType().equals("operator")){
            operators.add((TourOperator) newCompany);
        }
        else if(newCompany.getType().equals("agency")){
            agencies.add((TravelAgency) newCompany);
        }
    }

    public ArrayList<TourOperator> getOperators() {
        return operators;
    }

    public void setOperators(ArrayList<TourOperator> operators) {
        ApplicationDataStub.operators = operators;
    }

    public ArrayList<TravelAgency> getAgencies() {
        return agencies;
    }

    public TourOperator searchOperatorByName(String requiredOperatorName){
        
        
        for( TourOperator operator : operators ){
                      
            String operatorName = operator.getName();
            if( operatorName != null &&
                operatorName.equals(requiredOperatorName)){
                return operator;
            }
        }
        
        return null;
    }

    public static ArrayList<Claim> getClaims() {
        return claims;
    }

    public static void setClaims(ArrayList<Claim> claims) {
        ApplicationDataStub.claims = claims;
    }
    
    public String findCompanyNameById(Integer id){
        for(Company company : companies){
            
            if( company.getId().equals(id) ){
                return company.getName();
            }
        }
        
        return null;
    }
    
    private String findCompanyTypeByCompanyName(String name){
        for(Company company : companies){
            
            if( company.getName().equals(name) ){
                return company.getType();
            }
        }
        
        return null;
    }
    
    public ArrayList<Claim> findClaimsByCompanyName(String companyName, ClaimType type ){
        
        ArrayList<Claim> result = new ArrayList<Claim>();
                
        for(Claim claim : claims){
            String recipientName = claim.getTourOwnerName();
            String requesterName = claim.getRequesterName();
            boolean isOutputClaim = requesterName.equals(companyName) &&
                  (  type == ClaimType.FROM_COMPANY || type == ClaimType.ALL);
            
            boolean isInputClaim = recipientName.equals(companyName) &&
                    ( type == ClaimType.FOR_COMPANY || type == ClaimType.ALL );
            
            if( isOutputClaim || isInputClaim ){
                
                result.add(claim);                
            }
        }
        
        return result;
    } 
    
    public Claim findClaimById( Integer id ){
        
        for( Claim claim : claims ){
            if( claim.getId().equals(id) ){
                return claim;
            }
        }
        return null;
    }
    
    public Tour findTourById(Integer id){
        
        for( Tour tour : tours ){
            if( tour.getId().equals(id) ){
                return tour;
            }
        }
        return null;
    }
    
    
}
