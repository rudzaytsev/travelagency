/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

/**
 * @class
 * 
 * Rest client
 * 
 * @author rudolph
 */
public class RestClient {
   
    private String url;
    
    private String user;
    private String password;
    
    private RequestParams requestParams;
    private Response responseData;
    
    /**
     * Constructor
     */
    public RestClient(){        
    }
    
    /**
     * Gets url of service where to send requests
     * @return url of service where to send requests
     */
    public String getUrl() {
        return url;
    }

    /** 
     * Sets url of service where to send requests 
     * 
     * @param url url of service where to send requests 
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Gets user string id
     * @return user string id
     */
    public String getUser() {
        return user;
    }

    /**
     * Sets user string id to be set
     * @param user user string id to be set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * Retrives password string
     * @return password string
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password string
     * 
     * @param password password string
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets request parameters 
     * @return request parameters 
     */
    public RequestParams getRequestParams() {
        return requestParams;
    }

    /**
     * Sets request parameters 
     * @param requestParams request parameters 
     */
    public void setRequestParams(RequestParams requestParams) {
        this.requestParams = requestParams;
    }

    /**
     * Gets response data
     * @return response data
     */
    public Response getResponseData() {
        return responseData;
    }

    /**
     * Sets response data
     * @param responseData response data
     */
    public void setResponseData(Response responseData) {
        this.responseData = responseData;
    }
        
    
    /**
     * Sends post request to service with fixed url 
     * 
     * 
     * @return response string
     */
    public String postRequest(){
        try {
            
            HttpPost request = new HttpPost(url);
            this.basicAuth(request);
                       
            StringEntity myEntity = new StringEntity(requestParams.getRequestBody(),Charset.forName("UTF-8"));
            myEntity.setContentType(requestParams.getContentType());
            request.setEntity(myEntity);
     
            HttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(request);
     
            responseData = new Response();
            responseData.setStatusCode(response.getStatusLine().getStatusCode());
            responseData.setReason(response.getStatusLine().getReasonPhrase());
            
            String content = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
            responseData.setResponseBody(content);
            EntityUtils.consume(myEntity);

            System.out.println(content);
            
            return content;
        }    
        catch (ClientProtocolException ex) {
            Logger.getLogger(RestClient.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        catch (IOException ex) {
            Logger.getLogger(RestClient.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
    }
    
    /**
     * Basic Authentication using user & password values
     * 
     * @param request instance of HttpRequest
     */
    private void basicAuth(HttpRequest request){
        
         String auth = user + ":" + password;
         byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
         String authHeader = "Basic " + new String(encodedAuth);
         request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
         request.addHeader("version", "2");
         request.addHeader(HttpHeaders.CONTENT_TYPE, requestParams.getContentType());    
        
    }
    
    
    /**
     * @class RequestParams
     * 
     * Represents response params
     */
    public class RequestParams {
        
        /**
         * Constructor
         */
        public RequestParams(){            
        }
        
        private String contentType;
        private String version;
        private String requestBody;
        
        /**
         * Sets content type
         * @param cType content type string
         */
        public void setContentType(String cType){
            contentType = cType;
        }
        /**
         * Retrives content type string
         * @return content type string 
         */
        public String getContentType(){
            return contentType;
        }
        
        /**
         * Sets version
         * @param ver version to be set 
         */
        public void setVersion(String ver){
            version = ver;
        }
        
        /**
         * Retrives version
         * @return version 
         */
        public String getVersion(){
            return version;
        }
        
        /**
         * Sets request body
         * 
         * @param body request body to be set
         */
        public void setRequestBody(String body){
            requestBody = body;
        }
        
        /**
         * Retrives request body
         * @return request body
         */
        public String getRequestBody(){
            return requestBody;
        }               
        
    }
    
    /**
     * @class Response
     * 
     * Response to request for service
     * Included response body, status code and error reason.
     * 
     */
    public class Response {
        
        private int statusCode;
        private String reason;
        private String responseBody;
        
        /**
         * Constructor
         */
        private Response(){            
        }
        
        /**
         * Sets status code
         * @param code status code 
         */
        public void setStatusCode(int code){
            statusCode = code;
        }
        
        /**
         * Retrive status code
         * 
         * @return status code 
         */
        public int getStatusCode(){
            return statusCode;
        }
        
        /**
         * Sets error reason
         * 
         * @param rsn error reason 
         */
        public void setReason(String rsn){
            reason = rsn;
        }
        
        /**
         * Retrives error reason description
         * 
         * @return error reason description 
         */
        public String getReason(){
            return reason;
        }
        
        /**
         * Sets response body
         * 
         * @param respBody 
         */
        public void setResponseBody(String respBody){
            responseBody = respBody;
        }
        
        /**
         * Retrives response body
         * @return response body 
         */
        public String getResponseBody(){
            return responseBody;
        }
    }
    
    
}
