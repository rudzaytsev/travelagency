/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 * @class TwoTuple<A,B> 
 * Represents unwritable pair of objects
 * 
 * @author rudolph
 */
public class TwoTuple<A,B> {
    
    public final A first;
    public final B second;
    
    /**
     * Constructor
     * 
     * @param first first pair element
     * @param second second  pair element
     */
    public TwoTuple(A first, B second){
        this.first = first;
        this.second = second;
    }
    
    /**
     * Retrives string representation of pair
     * @return string representation of pair 
     */
    public String toString(){
        return "(" + first.toString() + "," + second.toString() + ")";
    }
}
