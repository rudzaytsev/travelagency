/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @class FpTools
 * 
 * Representing functional programming tools emulation in java 7
 * 
 * @author rudolph
 */
public class FpTools {
    
    /**
     * Interface to pass function as parameter
     * 
     * @param <A> generic type of source collection
     * @param <B> generic type of destination collection
     */
    public static interface Function<A,B> {
        
        public B invoke(A a);
    }
    
    /**
     * Map function. Map transforms source collection with
     * elements of type <A> to collection with elements of type <B>
     * using function func
     * 
     * @param <A> type of  source collection
     * @param <B> type of result collection
     * @param func function to transform source collection to collection with elements of type <B>
     * @param source source collection with elements of type <A>
     * @return result collection with with elements of type <B>
     */
    public static <A,B> Collection<B>  map(Function<A,B> func,
                            Collection<A> source){
        
        Collection<B> result = new ArrayList<B>();
        for(A a : source ){
            
           result.add(func.invoke(a));
        }
        
        return result;
    }
    
            
}
