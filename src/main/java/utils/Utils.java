/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import com.mycompany.travelagency.logic.Company;
import com.mycompany.travelagency.logic.TravelAgency;
import com.mycompany.travelagency.repository.CompanyRepository;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @class Utils
 * Represents important constants to configure
 * And utility methods which are used across the application
 * 
 * @author rudolph
 */
public final class Utils {
    
    public static final String DATASOURCE_URL =  //"http://192.168.4.250:8080/TourBooking/webresources/service";
            "http://testbooking.salesplatform.ru:8080/TourBooking/webresources/service";
    
    public static final String USER_TAG = "user";
    public static final String PASSWD_TAG = "passwd";
    
    /**
     * Gets current http session
     * @return HttpSession instance
     */
    public static HttpSession getSession(){
        
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true); // true == allow create
    }
    
    /**
     * Logout method. This method makes current http session invalid
     * 
     */
    public static void logout(){
        getSession().invalidate();
    }
    
    /**
     * Gets user company id as string
     * 
     * @return  user company id as string
     */
    public static String getUserIdAsStr(){
        return (String) getSession().getAttribute(USER_TAG);
    }
    
    
    /**
     * Gets user company id as long
     * 
     * @return Gets user company id as long
     */
    public static long getUserIdAsLong(){
        return Long.parseLong(getUserIdAsStr());
    }
    
    /**
     * Gets password of company as sting
     * 
     * @return password of company as sting
     */
    public static String getPasswdAsStr(){
        return (String) getSession().getAttribute(PASSWD_TAG);
    }
    
    /**
     * Gets password of company as long
     * 
     * @return password of company as long 
     */
    public static long getPasswdAsLong(){
        return Long.parseLong(getPasswdAsStr());
    }
    /**
     * Maps company list to agency list
     * 
     * @param companyList list of companies to map to agencies
     * @return agency list 
     */
    public static List<TravelAgency> toAgencies(List<Company> companyList){
        List<TravelAgency> result = new ArrayList<TravelAgency>();
        for(Company company : companyList){
            TravelAgency ta = (TravelAgency) company;
            result.add(ta);
        }
        return result;
    }
    
    /**
     * Check is the company agency ?
     * @param company for checks type
     * @return true if logged agency, otherwise false 
     */
    public static boolean isLoggedAgency(Company company){    
        return company instanceof TravelAgency;
    }
    
    public static boolean isLoggedAgency(){
        
        CompanyRepository repository = new CompanyRepository();
        Company currentCompany = repository.findCurrentCompany();
        return isLoggedAgency(currentCompany); 
    }
    
    /**
     * Gets currentDate in format: yyyy-MM-dd HH:mm:ss 
     * @return currentDate in format: yyyy-MM-dd HH:mm:ss 
     */	
    public static String getCurrentDateTime(){
        
        Calendar calendar = new GregorianCalendar();				
        String currentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());		
        return currentDate;

    }
    
    /**
     * Gets currentDate in format: yyyy-MM-dd
     * 
     * @return currentDate in format: yyyy-MM-dd
     */	
    public static String getCurrentDate(){
        
        Calendar calendar = new GregorianCalendar();				
        String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());		
        return currentDate;
    }
    
    /**
     * Generates TourOperatorTourId
     * as currentDate in format yMMddHHmmss mod 10000000000l
     * @return  generated TourOperatorTourId 
     */
    public static long generateTourOperatorTourId(){
        Calendar calendar = new GregorianCalendar();				
        String currentDate = new SimpleDateFormat("yMMddHHmmss").format(calendar.getTime());
       
        return Long.parseLong( currentDate) % 10000000000l;
    }

    /**
     * Gets current number of milliseconds since 1970.01.01 
     * @return current number of milliseconds since 1970.01.01 
     */
    public static long getCurrentTimeInMills(){
        Calendar calendar = new GregorianCalendar();
        return calendar.getTimeInMillis();
    }
    
}
