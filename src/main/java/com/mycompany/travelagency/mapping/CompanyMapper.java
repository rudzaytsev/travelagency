/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.mapping;

import com.mycompany.travelagency.logic.Company;
import com.mycompany.travelagency.logic.CompanyCreator;
import com.mycompany.travelagency.logic.DomainObject;
import com.mycompany.travelagency.repository.CompanyRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author rudolph
 */
public class CompanyMapper extends AbstractMapper<Company> {
    
    private static CompanyMapper mapper = new CompanyMapper();
    
    /**
     * 
     * @return  CompanyMapper instance
     */
    public static CompanyMapper getMapper(){
        return mapper;
    }

    /**
     * 
     * @throws UnsupportedOperationException
     * @param dObject
     * @return 
     */
    @Override
    public int insert(DomainObject dObject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Retrives Company instance
     * 
     * @param dataSourceObject data for parsing 
     * @return Company instance or null
     */
    @Override
    public Company createObject(String dataSourceObject) {
        
        Company company = null;
         
        JSONParser parser = new JSONParser();
                  
         
         try {
                         
               JSONObject object = (JSONObject) parser.parse(dataSourceObject);
               boolean hasErrorMsg = (object.get("errorMessage") != null);  
               if(hasErrorMsg){
                   return null;                   
               }
               
               long id = Long.parseLong(object.get("companyId").toString());
               String companyName = object.get("companyName").toString();
               
               int companyType = Integer.parseInt(object.get("companyType").toString());
               CompanyCreator creator  =  new CompanyCreator();
               company = creator.createCompany(companyType);               
               
               company.setId(id);
               company.setName(companyName);
               this.saveDomainObject(id, company);
         }
         catch (ParseException ex) {
               Logger.getLogger(CompanyMapper.class.getName()).
               log(Level.SEVERE, "Parse Exceptions occurred", ex);
         }
        
         return company;
    }

    /**
     * Retrives list of Company instances
     * 
     * @param dataSourceObject data to parse 
     * @return list of Company instances
     */
    @Override
    public List createAll(String dataSourceObject) {
        ArrayList list = new ArrayList<Company>();
        
        //json parsing and creating list of companies
                
        System.out.println(dataSourceObject);
        
        
        Company company = null;
         
        JSONParser parser = new JSONParser();
                  
         
         try {
            
               JSONObject object = (JSONObject) parser.parse(dataSourceObject);
               JSONArray jArray = (JSONArray) object.get("findCompanyResult");
               if(jArray != null){
                   
                   System.out.println("IS FOUND");
                   
                   System.out.println("JARRAY SIZE = " + jArray.size());

                   for(int i = 0; i < jArray.size(); i++){
                       JSONObject obj = (JSONObject) jArray.get(i);
                       
                       long id = Long.parseLong(obj.get("companyId").toString());
                       
                       if(this.isLoadedDomainObject(id)){
                           company = this.loadDomainObject(id);
                       }
                       else {
                            int companyType = Integer.parseInt(obj.get("companyType").toString());
                            CompanyCreator creator  =  new CompanyCreator();
                            company = creator.createCompany(companyType);

                            company.setId(id);
                            company.setName(obj.get("companyName").toString());
                            this.saveDomainObject(id, company);
                       
                       }
                       
                       list.add(company);

                   }

               }
              
             
            System.out.println("END WHILE");

            
         } catch (ParseException ex) {
            Logger.getLogger(CompanyMapper.class.getName()).
                   log(Level.SEVERE, "Parse Exceptions occurred", ex);
         }

          for(Company c : (ArrayList<Company>) list){
            System.out.println("Company name = " + c.getName());
            System.out.println("Company type = " + c.getType());
            System.out.println("Company id = " + c.getId());
          }
        
        
        return list;
    }

       
    /**
     * 
     * @throws UnsupportedOperationException
     * @param domainObject 
     * @return 
     */
    @Override
    public long update(Company domainObject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * 
     * @throws UnsupportedOperationException
     * @param domainObject
     * @return 
     */
    @Override
    public DomainObject delete(Company domainObject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * 
     * @throws UnsupportedOperationException
     * @param dataSourceObject
     * @return 
     */
    @Override
    public Company createObject(Object dataSourceObject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Finds Company by id 
     * 
     * @param id id to find company
     * @return Company instance or null
     */
    @Override
    public Company find(Long id) {
        if(isLoadedDomainObject(id)){
            return this.loadDomainObject(id);
        }
        CompanyRepository repository = new CompanyRepository();
        
        return repository.findCompanyById(id);
    }

   

   
    
}
