/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.mapping;

import com.mycompany.travelagency.logic.Claim;
import com.mycompany.travelagency.logic.Customer;
import com.mycompany.travelagency.logic.DomainObject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


/**
 * @class ClaimMapper
 * 
 * 
 * @author rudolph
 */
public class ClaimMapper extends AbstractMapper<Claim> {
    
    private static ClaimMapper mapper = new ClaimMapper();
    
    /**
     * Retrives ClaimMapper instance
     * 
     * @return ClaimMapper instance 
     */
    public static ClaimMapper getMapper(){
        return mapper;
    }

    /**
     * 
     * @throws UnsupportedOperationException
     * @param dObject
     * @return 
     */
    @Override
    public int insert(DomainObject dObject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * 
     * @throws UnsupportedOperationException
     * @param dataSourceObject
     * @return 
     */
    @Override
    public Claim createObject(String dataSourceObject) {
          throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Retrives Claim instance
     * 
     * @param dataSourceObject data for mapping to claim
     * @return Claim instance
     */
    @Override
    public Claim createObject(Object dataSourceObject) {
        
        Claim claim = null;
        
        JSONObject object = (JSONObject) dataSourceObject;
        long id = Long.parseLong(object.get("claimId").toString());
        if(this.isLoadedDomainObject(id)){
            return this.loadDomainObject(id);
        }
        else {
            claim = new Claim();
            claim.setId(id);
            String requestDate = object.get("requestDate").toString();
            claim.setRequestDate(requestDate);
            long tourOwnerId = Long.parseLong(object.get("tourOwnerId").toString());
            long requesterId = Long.parseLong(object.get("requesterId").toString());
            long tourId = Long.parseLong(object.get("tourId").toString());
            long tourOperatorTourId = Long.parseLong(object.get("tourOperatorTourId").toString());
            
            claim.setRequestDate(requestDate);
            claim.setRequesterId(requesterId);
            claim.setTourOwnerId(tourOwnerId);
            claim.setTourId(tourId);
            claim.setTourOperatorTourId(tourOperatorTourId);
            
            // Customers mapping
            ArrayList<Customer> customers = this.createCustomers(object);
            claim.setCustomers(customers);
            
            this.saveDomainObject(id, claim);     
            
        }
        
        return claim;
    }

    /**
     * Retrives list of claims
     * 
     * @param dataSourceObject data for mapping to claims list
     * @return list of claims
     */
    @Override
    public List createAll(String dataSourceObject) {
        
        ArrayList list = new ArrayList<Claim>();
        
        //json parsing and creating list of companies
                
        //System.out.println(dataSourceObject);        
        
        Claim claim = null;         
        JSONParser parser = new JSONParser();                  
         
         try {
            
               JSONObject object = (JSONObject) parser.parse(dataSourceObject);
               JSONArray jArray = (JSONArray) object.get("findClaimsResult");
               if(jArray != null){
                   
                   System.out.println("IS FOUND Claim");                   
                   System.out.println("JARRAY CLAIMS SIZE = " + jArray.size());

                   for(int i = 0; i < jArray.size(); i++){
                       
                       claim = this.createObject(jArray.get(i));                      
                       list.add(claim);
                   }
               }
              
             
               System.out.println("END WHILE");

            
         } catch (ParseException ex) {
             Logger.getLogger(CompanyMapper.class.getName()).
                   log(Level.SEVERE, "Parse Exceptions occurred", ex);
         }

         /*
          for(Claim c : (ArrayList<Claim>) list){
            System.out.println("Claim id = " + c.getId());
            System.out.println("Claim requestDate = " + c.getRequestDate());
            System.out.println("Requester id = " + c.getRequesterId());
            System.out.println("tourOwnerId id = " + c.getTourOwnerId());
            System.out.println("tour id = " + c.getTourId());
          }
        */
        
        return list;
    }
    
    /**
     * @throws UnsupportedOperationException
     * 
     * @param id to find domain object
     * @return Claim instance
     */
    @Override
    public Claim find(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @throws UnsupportedOperationException
     * 
     * @param domainObject
     * @return 
     */
    @Override
    public long update(Claim domainObject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @throws UnsupportedOperationException
     * 
     * @param domainObject
     * @return 
     */
    @Override
    public DomainObject delete(Claim domainObject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * 
     * 
     * @param obj JSON Object data to create customer
     * @return Customer instance
     */
    private Customer createCustomer(Object obj){
        
        Customer customer = new Customer();        
        JSONObject object = (JSONObject) obj;
        
        String ruName = getNotNullField(object,"ruName");
        ruName = getDefinedRuName(ruName,object);
        
        String sex = getNotNullField(object,"sex");
        
        String phone = getNotNullField(object,"phone");
        phone = getDefinedPhone(phone, object);
        
        String birthDate = getNotNullField(object,"birthDate");
        String transborderName = getNotNullField(object,"transborderName");
        String transborderPassport = getNotNullField(object,"transborderPassport");
        
        String transborderFromDate = getNotNullField(object,"transborderFromDate");
        String transborderToDate = getNotNullField(object,"transborderToDate");
        
        customer.setRuName(ruName);
        customer.setBirthDate(birthDate);
        customer.setSex(sex);
        customer.setPhone(phone);
        customer.setTransborderName(transborderName);
        customer.setTransborderPassport(transborderPassport);
        customer.setTransborderFromDate(transborderFromDate);
        customer.setTransborderToDate(transborderToDate);
        
        
        return customer;
    }
    
    /**
     * Retrives not null field value from JSONObject
     * 
     * @param struct JSONObject to get field
     * @param fieldName name of field in struct 
     * @return string value from json or  Customer.DEFAULT_MSG
     */
    private String getNotNullField(JSONObject struct,String fieldName){
        
        Object obj = struct.get(fieldName);
        
        return (obj == null) ? Customer.DEFAULT_MSG: obj.toString();
    }
    
    
    /**
     * Retrives defined ruName
     * 
     * @param ruName string to check
     * @param object to parse for lastname, firstname or other personal info to build ruName
     * @return built ruName
     */
    private String getDefinedRuName(String ruName,JSONObject object){
        if(ruName.equals(Customer.DEFAULT_MSG)){
            String firstName = getNotNullField(object,"firstName");
            String lastName = getNotNullField(object,"lastName");
            StringBuilder builder = new StringBuilder();
            if(!lastName.equals(Customer.DEFAULT_MSG)){
                
                builder.append(lastName);
                if(!firstName.equals(Customer.DEFAULT_MSG)){
                    builder.append(" ");
                    builder.append(firstName);                    
                }
                
                
                return builder.toString();
            }
            
            String aFirstName = getNotNullField(object,"FirstName");
            String aLastName = getNotNullField(object,"LastName");
                      
            if(!aLastName.equals(Customer.DEFAULT_MSG)){
                
                builder.append(aLastName);
                if(!aFirstName.equals(Customer.DEFAULT_MSG)){
                    builder.append(" ");
                    builder.append(aFirstName);                   
                }
                
                return builder.toString();
            }
        }
        return ruName;
    }
    
    /**
     * Retrives defined phone value
     * 
     * @param phone string to check for definition
     * @param object object to parse to get from phone information 
     * @return new defined phone string value or Customer.DEFAULT_MSG
     */
    private String getDefinedPhone(String phone, JSONObject object){
        if(phone.equals(Customer.DEFAULT_MSG)){
            phone = getNotNullField(object,"phone");            
        }
        if(phone.equals(Customer.DEFAULT_MSG)){
            phone = getNotNullField(object,"Phone");            
        }
        if(phone.equals(Customer.DEFAULT_MSG)){
            phone = getNotNullField(object,"mobile");            
        }
        if(phone.equals(Customer.DEFAULT_MSG)){
            phone = getNotNullField(object,"Mobile");            
        }
        
        return phone;
    }    
    
    /**
     * Retrives list of customers
     * 
     * @param obj JSONObject instance to parse for customers data 
     * @return list of customers
     */
    private ArrayList<Customer> createCustomers(Object obj){
        
         JSONObject object = (JSONObject) obj;
        
         String str = object.get("customerData").toString();
         
         //System.out.println("Customers JSON STR = " + str);
     
         JSONParser parser = new JSONParser();
         ArrayList customers = new ArrayList<Customer>();
         Customer customer = null;          

         JSONArray customersData;
         try {         
                try {
                   customersData = (JSONArray) parser.parse(str);
                } catch (ParseException ex) {
                   Logger.getLogger(ClaimMapper.class.getName()).log(Level.SEVERE, null, ex);

                   System.out.println("PARSE EXCEPTION !!!");

                   Object valueObj = JSONValue.parse(str);
                   Object yetAnotherObj = JSONValue.parse(valueObj.toString());
                   customersData = (JSONArray) yetAnotherObj;
                }
        
                
                if(customersData != null){

                          //System.out.println("IS FOUND Customer");                   
                          //System.out.println("JARRAY Customers SIZE = " + customersData.size());

                          for(int i = 0; i < customersData.size(); i++){

                              System.out.println("INSIDE");
                              customer = this.createCustomer(customersData.get(i));                      
                              customers.add(customer);

                              System.out.println("Customer Name = " + customer.getRuName());
                          }
                }

         }          
         catch (ClassCastException ex){
             JSONObject customersDataObj;
             try {
                    customersDataObj = (JSONObject) parser.parse(str);
             
                    if(customersDataObj != null){

                          //System.out.println("IS FOUND Customer Object");
                          customer = this.createCustomer(customersDataObj);
                          //System.out.println("Customer Name = " + customer.getRuName());
                          customers.add(customer);
                     }
             } catch (ParseException ex1) {
                 Logger.getLogger(ClaimMapper.class.getName()).log(Level.SEVERE, null, ex1);
             }
         }
        
         return customers;
    }
    
    
}
