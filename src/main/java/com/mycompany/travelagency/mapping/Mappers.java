/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.mapping;

import com.mycompany.travelagency.logic.Claim;
import com.mycompany.travelagency.logic.Company;
import com.mycompany.travelagency.logic.DomainObject;
import com.mycompany.travelagency.logic.Tour;
import com.mycompany.travelagency.logic.TourOperator;
import java.util.IdentityHashMap;
import java.util.Map;

/**
 * @class 
 * This class in used to find mappers by metadata
 * 
 * @author rudolph
 */
public class Mappers {
    
    public static final Map< Class< ? extends DomainObject> ,AbstractMapper< ? extends DomainObject> > registry 
            = new IdentityHashMap<Class< ? extends DomainObject> ,AbstractMapper< ? extends DomainObject> >();
    
    static {
        
        registry.put(Company.class, CompanyMapper.getMapper() );
        registry.put(Claim.class, ClaimMapper.getMapper());
        registry.put(Tour.class, TourMapper.getMapper());
        
    }
    
    /**
     * 
     * @param <T> generic type to determine
     * @param klass Class<T> instance that is the key to get mapper as AbstractMapper<T>
     * @return mapper as AbstractMapper<T>
     */
    public static < T extends DomainObject> AbstractMapper<T> getMapperForClass(Class<T> klass){
        return (AbstractMapper<T>) registry.get(klass);
    }
    
}
