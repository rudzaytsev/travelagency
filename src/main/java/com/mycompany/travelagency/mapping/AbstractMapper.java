/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.mapping;

import com.mycompany.travelagency.logic.DomainObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @class AbstractMapper< T extends DomainObject >
 * Generic class is base for all mappers
 * 
 * @author rudolph
 */
public abstract class AbstractMapper< T extends DomainObject > {
    
    protected Map<Long, T> loadedMap = new HashMap<Long,T>();
    
    /*
    *
    * This method inserts Domain Object into DataBase
    * and returns new id of Domain Object form DB
    * 
    */ 
    public abstract int insert(DomainObject dObject);
    
   /**
    * This method selects Domain Object from DataBase
    * @param dataSourceObject data to create object
    * @return instance of class that extends DomainObject
    * 
    */ 
    public abstract T createObject(String dataSourceObject);
    /**
     * 
     * @param dataSourceObject data to create object
     * @return instance of class that extends DomainObject
     */
    public abstract T createObject(Object dataSourceObject);
    
    public abstract List createAll(String dataSourceObject);
   
    /**
     * Finds Domain Object by id
     * 
     * @param id id of DomainObject instance to find
     * @return instance of class that extends DomainObject   
     */
    public abstract T find(Long id);
    
    public abstract long update( T  domainObject );
    
    public abstract DomainObject delete( T domainObject );
   
    /** 
     * Loads domain object from loaded map by id
     * 
     * @param id id to load domain object from map
     * @return instance of class that extends DomainObject if object is found, null otherwise
     */
    public T loadDomainObject(Long id){
        if(this.isLoadedDomainObject(id)){
           return loadedMap.get(id);
        }
        return null;
    }
    
    public boolean isLoadedDomainObject(Long id){
        return loadedMap.containsKey(id);
    }
    
    /**
     * Saves domain object in loaded map
     * 
     * @param id key to save domain object in map
     * @param domainObject to be saved in map
     */
    public void saveDomainObject(Long id,T domainObject){
        loadedMap.put(id, domainObject);
    }
    
}
