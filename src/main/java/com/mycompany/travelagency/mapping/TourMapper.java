/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.mapping;

import com.mycompany.travelagency.logic.DomainObject;
import com.mycompany.travelagency.logic.Tour;
import com.mycompany.travelagency.repository.TourRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @class TourMapper 
 * 
 * 
 * @author rudolph
 */
public class TourMapper extends AbstractMapper<Tour> {
    
    private static TourMapper mapper = new TourMapper();
    
    /**
     * Retrives TourMapper instance
     * 
     * @return TourMapper instance
     */
    public static TourMapper getMapper(){
        return mapper;
    }

    /**
     * 
     * @throws UnsupportedOperationException
     * @param dObject
     * @return 
     */
    @Override
    public int insert(DomainObject dObject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Retrives Tour instance
     * 
     * @param dataSourceObject data string to parse
     * @return Tour instance
     */
    @Override
    public Tour createObject(String dataSourceObject) {
        
        JSONParser parser = new JSONParser();
        Tour tour = new Tour();
        try {
            JSONObject obj = (JSONObject) parser.parse(dataSourceObject);
            
            if( obj.get("errorMessage") != null){
                //mapper.saveDomainObject(tour.getId(), tour);
                return tour;
            } 
            
            long tourId = getNotNullLongField(obj,"tourId");
            long tourOperatorTourId = getNotNullLongField(obj,"tourOperatorTourId");
            long parentTourId = getNotNullLongField(obj,"parentTourId");
           
            JSONObject tourData = (JSONObject) obj.get("tourData");
            long tourOperatorId = getNotNullLongField(tourData,"tourOperatorId");
            
            List<String> attributes = Tour.getAttributesList();
            for( String attribute  : attributes ){
                if(this.hasField(tourData, attribute)){
                    tour.setOptionalAttribute(attribute,
                                          getNotNullStrField(tourData,attribute));
                }
            }
                        
            tour.setId(tourId);
            tour.setTourOperatorTourId(tourOperatorTourId);
            
            tour.setParrentTourId(parentTourId);
            tour.setTourOperatorId(tourOperatorId);
            
            mapper.saveDomainObject(tourId, tour);
            
            
            
        } catch (ParseException ex) {
            Logger.getLogger(TourMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return tour;
        
    }
    
    /**
     * Checks does the concrete field exist in JSONObject struct
     * 
     * @param struct JSONObject to parse field
     * @param field name of field
     * @return true if has field and field is not empty, otherwise false
     */
    private boolean hasField(JSONObject struct,String field){
        Object obj = struct.get(field);
        return (obj != null) && !(obj.toString().equals(""));
    }
    
    /**
     * Retrives long value of concrete field
     * 
     * @param struct JSONObject to parse field
     * @param fieldName name of field
     * @return long value of field
     */
    private long getNotNullLongField(JSONObject struct,String fieldName){
        
        Object obj = struct.get(fieldName);        
        return (obj == null) ? 0: Long.parseLong(obj.toString());
    }
    
    /**
     * Retrives string value of concrete field
     * 
     * @param struct JSONObject to parse field
     * @param fieldName name of field
     * @return string value of field
     */
    private String getNotNullStrField(JSONObject struct,String fieldName){
        
        Object obj = struct.get(fieldName);        
        return (obj == null) ? "undefined": obj.toString();
    }

    /**
     * 
     * 
     * @throws UnsupportedOperationException
     * @param dataSourceObject 
     * @return 
     */
    @Override
    public Tour createObject(Object dataSourceObject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Retrives list of Tour instances
     * 
     * 
     * @param dataSourceObject data string to parse
     * @return list of Tour instances
     */
    @Override
    public List createAll(String dataSourceObject) {
        
        JSONParser parser = new JSONParser();
        List<Tour> tours = new ArrayList<Tour>();
        try {
            JSONObject obj = (JSONObject) parser.parse(dataSourceObject);
            
            if( obj.get("errorMessage") != null){
                
                return tours;
            }
            
            JSONArray toursArray = (JSONArray) obj.get("findToursResult");
           
            for(int i = 0; i < toursArray.size(); i++){
                JSONObject tourData = (JSONObject) toursArray.get(i);
                Tour someTour = new Tour();
                
                long tourId = getNotNullLongField(tourData,"tourId");
                long tourOperatorTourId = getNotNullLongField(tourData,"tourOperatorTourId");
                long tourOperatorId = getNotNullLongField(tourData,"tourOperatorId");
                
                List<String> attributes = Tour.getAttributesList();
                for( String attribute  : attributes ){
                    if(this.hasField(tourData, attribute)){
                        someTour.setOptionalAttribute(attribute,
                                              getNotNullStrField(tourData,attribute));
                    }
                }
                        
                someTour.setId(tourId);
                someTour.setTourOperatorTourId(tourOperatorTourId);
                someTour.setTourOperatorId(tourOperatorId);
                
                tours.add(someTour);
                mapper.saveDomainObject(tourId, someTour);                
            }
            
        
        }             
        catch (ParseException ex) {
            Logger.getLogger(TourMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return tours;
    }

    /**
     * Find Tour by id
     * 
     * 
     * @param id to find tour
     * @return Tour instance
     */
    @Override
    public Tour find(Long id) {
        TourRepository repository = new TourRepository();
        if(this.isLoadedDomainObject(id)){
            return (Tour) this.loadDomainObject(id);
        }        
        return repository.findTourByIdAsUser(id);
    }

    /**
     * 
     * 
     * @throws UnsupportedOperationException
     * @param domainObject
     * @return 
     */
    @Override
    public long update(Tour domainObject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * 
     * @throws UnsupportedOperationException
     * @param domainObject
     * @return 
     */
    @Override
    public DomainObject delete(Tour domainObject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
}
