/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.controllers;

import com.mycompany.travelagency.logic.Company;
import com.mycompany.travelagency.logic.Tour;
import com.mycompany.travelagency.logic.TourOperator;
import com.mycompany.travelagency.mapping.TourMapper;
import com.mycompany.travelagency.repository.CompanyRepository;
import com.mycompany.travelagency.repository.TourRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import stubs.ApplicationDataStub;

/**
 * @class TourInfoController
 * 
 * This controller handles requests for viewing tour information
 * 
 * @author rudolph
 */
@RequestMapping(value = "/**/tourinfo")
@Controller
public class TourInfoController {
    
    /**
     * Constructor
     * 
     * 
     */    
    public TourInfoController(){        
    }
    
    /**
     * This method fills the model by Tour instance, and show tour information
     * 
     * 
     * @param id id of tour to show information
     * @param model model of domain i.e. key-value storage of entities to view and fill in jsp pages
     * @return name of jsp page to view
     */    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String showInfo(@PathVariable("id") Long id,  Model model){
       
       
        Tour tour = this.getSelectedTour(id);
        
        model.addAttribute("tour", tour);
        
        return "tourinfo";
    }
    
    /**
     * This method retrives selected tour by id
     * 
     * 
     * @param id id of selected tour
     * @return tour which has selected id
     */
    public Tour getSelectedTour(Long id){
        
        CompanyRepository companyRepository = new CompanyRepository();
        Company currentCompany = companyRepository.findCurrentCompany();
        
        if(currentCompany instanceof TourOperator){
            TourMapper mapper = TourMapper.getMapper();
            if(mapper.isLoadedDomainObject(id)){
                
                System.out.println("************[IS LOADED TOUR]************************************");
                Tour loadedTour = mapper.loadDomainObject(id);
                long currentCompanyId = currentCompany.getId();
                System.out.println("Operator Id = " + loadedTour.getTourOperatorId());
                System.out.println("Current company Id = " + currentCompanyId);
                if(loadedTour.getTourOperatorId() == currentCompanyId){
                    return loadedTour;
                }                
            }            
        }
        
        System.out.println("************[OTHER BRANCH]************************************");
        
        TourRepository repository = new TourRepository();
        Tour tour = repository.findTourByIdAsUser(id);
        
        return tour;        
    }
    
}
