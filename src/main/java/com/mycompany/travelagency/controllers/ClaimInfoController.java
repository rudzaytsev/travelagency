/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.controllers;

import com.mycompany.travelagency.logic.Claim;
import com.mycompany.travelagency.logic.ClaimType;
import com.mycompany.travelagency.repository.ClaimRepository;
import java.util.ArrayList;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import stubs.ApplicationDataStub;

/**
 * @class ClaimInfoController
 * 
 * This class represents a controller that handle requests 
 * for viewing concrete claims
 * 
 * 
 * @author rudolph
 */
@RequestMapping(value = "/**/claiminfo")
@Controller
public class ClaimInfoController {
    
    /**
     * 
     * Constructor     
     * 
     */    
    public ClaimInfoController(){        
    }
    
    /**
     * 
     * @param id id of claim to show full claim information
     * @param model model of domain i.e. key-value storage of entities to view and fill in jsp pages
     * @return name of jsp page to view
     */    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String show(@PathVariable("id") Long id,  Model model){
        
        model.addAttribute("id", id);       
        
        ClaimRepository claimRepository = new ClaimRepository();
        Claim claim = claimRepository.findClaimById(id);        
       
        model.addAttribute("claim", claim);                    
                
        return "claiminfo";
    }
    
}
