/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.controllers;

import com.mycompany.travelagency.logic.Claim;
import com.mycompany.travelagency.logic.Company;
import com.mycompany.travelagency.logic.CompanyCreator;
import com.mycompany.travelagency.logic.CompanyType;
import com.mycompany.travelagency.logic.EntityDeleteParams;
import com.mycompany.travelagency.logic.TourOperator;
import com.mycompany.travelagency.logic.TravelAgency;
import com.mycompany.travelagency.logic.User;
import com.mycompany.travelagency.repository.ClaimRepository;
import com.mycompany.travelagency.repository.CompanyRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import stubs.ApplicationDataStub;
import utils.Utils;
import view.ClaimAddParams;


/**
 * 
 * @class WelcomeController
 * 
 * This controller handles login and logout requests
 * 
 * @author rudolph
 */
@Controller
public class WelcomeController  {
    
    
    /**
     * 
     * Constructor
     * 
     */
    public WelcomeController() {
    }
    
    
    /**
     * This method invokes when client connects to web-server where runs this application 
     * 
     * @param model model of domain i.e. key-value storage of entities to view and fill in jsp pages
     * @return name of jsp page to view
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model){        
        
        model.addAttribute("user", new User());
        System.out.println("Index method called");
        
                
        return "index";
    }
    
    
    /**
     * 
     * 
     * @param session current user session
     * @param user User instance
     * @param model model of domain i.e. key-value storage of entities to view and fill in jsp pages
     * @return name of jsp page to view
     */
    @RequestMapping(method = RequestMethod.POST)
    public String loginAdmin(HttpSession session,User user,Model model){
        
        System.out.println("Login Admin() called");
        System.out.println("Login: " + user.getLogin());
        System.out.println("Pass: " + user.getPassword());
        Long companyId;
        try{
             companyId = Long.parseLong(user.getLogin());
        }
        catch( NumberFormatException ex ){
             System.out.println("NumberFormatException ex");
             return "index";
        }
                 
        CompanyRepository companyRepository = new CompanyRepository();
        session.setAttribute(Utils.USER_TAG,user.getLogin());
        session.setAttribute(Utils.PASSWD_TAG,user.getPassword());
        
        Company currentCompany = companyRepository.findCompanyById(companyId);
        if(currentCompany == null){
            System.out.println("Current Company not Found");
            return "index";
        }

        ClaimRepository claimRepository = new ClaimRepository();
        List<Claim> allClaims = claimRepository.findClaimsByCompany(currentCompany);            
        model.addAttribute("currentCompany",currentCompany);
        model.addAttribute("loggedAgency", Utils.isLoggedAgency(currentCompany));
        model.addAttribute("allClaims", allClaims);
        model.addAttribute("claimAddParams", new ClaimAddParams());
        model.addAttribute("claimDeleteParams", new EntityDeleteParams());

        return "claims";
      
        
    }
    
    /**
     * Logout method
     * 
     * 
     * @param model model of domain i.e. key-value storage of entities to view and fill in jsp pages
     * @return name of jsp page to view
     */
    @RequestMapping( value = "/**/logout", method = RequestMethod.GET)
    public String logout(Model model){
        
        Utils.logout();
        model.addAttribute("user", new User());
        
        return "index";
    }
    
    

}
        
