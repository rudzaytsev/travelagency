/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.logic;

/**
 *
 * Represents tour type enum
 * 
 * @author rudolph
 */
public enum TourType {
    
    TOUR, HOTEL, ALL
}
