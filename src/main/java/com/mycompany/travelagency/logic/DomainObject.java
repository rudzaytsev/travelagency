/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.logic;

/**
 * @class DomainObject
 * 
 * Represents object of domain model
 * This class is base for other entities of domain model
 * 
 * @author rudolph
 */
public abstract class DomainObject {
    
    private Long id;
    
    /**
     * Constructor
     * 
     */
    public  DomainObject(){        
    }

    /**
     * Retrives id of domain object
     * 
     * @return id of domain object 
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id of domain object
     * 
     * @param id id that will be assigned to domain object 
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    
    
}
