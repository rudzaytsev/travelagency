/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.logic;

/**
 * @class EntityDeleteParams
 * 
 * Class encapsulates id of entity to remove from database
 * 
 * @author rudolph
 */
public class EntityDeleteParams {
    
    public long deleteId;

    /**
     * Retrives the id of domain object to remove from database
     * 
     * @return id of domain object to remove from database 
     */
    public long getDeleteId() {
        return deleteId;
    }

    /**
     * 
     * @param deleteId id of domain Object to remove from database
     */
    public void setDeleteId(long deleteId) {
        this.deleteId = deleteId;
    }
    
    
    
}
