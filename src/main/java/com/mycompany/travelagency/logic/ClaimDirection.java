/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.logic;

/**
 *
 * Claim direction enum
 * 
 * 
 * @author rudolph
 */
public enum ClaimDirection {
    
    FROM_AGENCY, FOR_OPERATOR
}
