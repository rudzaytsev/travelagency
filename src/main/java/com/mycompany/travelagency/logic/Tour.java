/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @class Tour
 * Represents tour
 * 
 * 
 * @author rudolph
 */
public class Tour extends DomainObject {
    
    public static final String DEFAULT_MSG = "undefined";
    private long tourOperatorTourId = 0;
        
    private String lastModified;
    private long parrentTourId = 0;
    
    private long tourOperatorId = 0;
    
    private Map<String,String> optionalAttributes;
    
    private static final List<String> availableAttributes = new ArrayList<String>();
        
    
    static {
        
        availableAttributes.add("name");        
        
        availableAttributes.add("beginDate");
        availableAttributes.add("endDate");
        
        availableAttributes.add("type");
        
        availableAttributes.add("country");
        availableAttributes.add("town");
        availableAttributes.add("hotel");
        availableAttributes.add("hotelCategory");
        
        availableAttributes.add("room");
        
        availableAttributes.add("duration");
        availableAttributes.add("accomodation");
        availableAttributes.add("aircompany");
        
        availableAttributes.add("food");
        availableAttributes.add("lastModified");               
        
        availableAttributes.add("priceAmount");
        availableAttributes.add("priceCurrency");
        availableAttributes.add("priceUnit");
        
        availableAttributes.add("description");
        
    }
    
    /**
     * Constructor
     * 
     * 
     */
    public Tour(){
        optionalAttributes = new LinkedHashMap();
        optionalAttributes.put("name", DEFAULT_MSG);
    }

    /**
     * Retrives id of tour in Tour operator's system
     * Often uses to delete tours from database
     * 
     * @return  id of tour in Tour operator's system
     */
    public long getTourOperatorTourId() {
        return tourOperatorTourId;
    }

    public void setTourOperatorTourId(long tourOperatorTourId) {
        this.tourOperatorTourId = tourOperatorTourId;
    }

    /**
     * Retrives last modified date as string
     * 
     * @return  last modified date as string
     */
    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    /**
     *  Retrives parrent tour Id
     * 
     * @return parrent tour Id
     */
    public long getParrentTourId() {
        return parrentTourId;
    }

    public void setParrentTourId(long parrentTourId) {
        this.parrentTourId = parrentTourId;
    }

    /**
     * 
     * Retrives map of optional attributes.
     * Attribute examples:
     * name, beginDate, endDate, type, country, town, hotel, hotelCategory, room,
     * duration, accomodation, aircompany, food, lastModified, priceAmount,
     * priceCurrency, priceUnit, description 
     * 
     * Key is attribute name, and value is attribute value
     * 
     * @return map of optional attributes  
     */
    public Map<String, String> getOptionalAttributes() {
        return optionalAttributes;
    }

    public void setOptionalAttributes(Map<String, String> optionalAttributes) {
        this.optionalAttributes = optionalAttributes;
    }
    
    /**
     * Sets/Adds  attribute to optional attributes map 
     * 
     * @param key key of optional attribute to send
     * @param val value of optional attribute to send
     */
    public void setOptionalAttribute(String key, String val){
        optionalAttributes.put(key, val);        
    }
    
    /**
     * Retrives optional attribute value by attribute key
     * 
     * @param key key to retrive attribute value
     * @return string value of optional attributes 
     */
    public String getOptionalAttribute(String key){
        return optionalAttributes.get(key);
    }
    
    /**
     * Removes attribute from map by key
     * 
     * @param key do remove attribute from map
     */
    public void removeOptionalAttribute(String key){
        optionalAttributes.remove(key);
    }
    
    /**
     * Check has map attribute with concrete key
     * 
     * @param attribKey key to check 
     * @return true if map has element with attribKey, otherwise false
     */
    public boolean hasAttribute(String attribKey){
       String val = optionalAttributes.get(attribKey);
       return val != null;
    }
    
    public String getName(){
        return this.hasAttribute("name") ?
                        this.getOptionalAttribute("name") :  DEFAULT_MSG;
    }
    
    /**
     * Retrives unique name of tour
     * Unique name of tour is built by concatination of tour Id,
     * separator i.e. --- and tour name
     * 
     * @return unique name of tour
     */
    public String getUniqueName(){
        StringBuilder builder = new StringBuilder();
        builder.append(this.getId());
        builder.append(" -- ");
        builder.append(this.getName());
        return builder.toString();
    }
    
    public String getBeginDate(){
        return this.hasAttribute("beginDate") ?
                        this.getOptionalAttribute("beginDate") :  DEFAULT_MSG;
    }
    
    public String getEndDate(){
         return this.hasAttribute("endDate") ?
                        this.getOptionalAttribute("endDate") :  DEFAULT_MSG;
    }
    
    public String getType(){
         return this.hasAttribute("type") ?
                        this.getOptionalAttribute("type") :  DEFAULT_MSG;
    }
    
    public String getCountry(){
         return this.hasAttribute("country") ?
                        this.getOptionalAttribute("country") :  DEFAULT_MSG;
    }
    
    public String getTown(){
         return this.hasAttribute("town") ?
                        this.getOptionalAttribute("town") :  DEFAULT_MSG;
    }
    
    public String getHotel(){
         return this.hasAttribute("hotel") ?
                        this.getOptionalAttribute("hotel") :  DEFAULT_MSG;
    }
    
    public String getDuration(){
         return this.hasAttribute("duration") ?
                        this.getOptionalAttribute("duration") :  DEFAULT_MSG;
    }
    
    public String getPriceAmount(){
         return this.hasAttribute("priceAmount") ?
                        this.getOptionalAttribute("priceAmount") :  DEFAULT_MSG;
    }
    
    public String getPriceCurrency(){
         return this.hasAttribute("priceCurrency") ?
                        this.getOptionalAttribute("priceCurrency") :  DEFAULT_MSG;
    }
    
    public String getPriceUnit(){
         return this.hasAttribute("priceUnit") ?
                        this.getOptionalAttribute("priceUnit") :  DEFAULT_MSG;
    }
    
    /**
     * Retrives list of avaliable optional attributes
     * 
     * @return list of avaliable optional attributes 
     */
    public static List<String> getAttributesList(){
        
        return availableAttributes; 
    }

    /**
     * Retrives tour operator id in SalesPlatform service 
     * 
     * @return tour operator id in SalesPlatform service 
     */
    public long getTourOperatorId() {
        return tourOperatorId;
    }

    public void setTourOperatorId(long tourOperatorId) {
        this.tourOperatorId = tourOperatorId;
    }
    
    
}
