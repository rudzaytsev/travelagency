/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.logic;

/**
 *
 * Claim type enum
 * 
 * 
 * @author rudolph
 */
public enum ClaimType {
    
    
    FOR_COMPANY, FROM_COMPANY, ALL
}
