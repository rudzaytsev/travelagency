/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.logic;

/**
 * 
 * @class  User
 * 
 * Represents user (registered company administrator)
 * 
 * @author rudolph
 */
public class User {
    
    private String login;
    private String password;
    
    /**
     * 
     * Constructor
     * 
     */
    public User(){        
    }

    /**
     * Retrives login as string
     * 
     * 
     * @return login as string 
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets login for user
     * 
     * @param login login to be set 
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Retrive password for user as string
     * 
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password for user
     * 
     * 
     * @param password password for user
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
