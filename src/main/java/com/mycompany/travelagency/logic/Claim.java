/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.logic;

import com.mycompany.travelagency.mapping.CompanyMapper;
import com.mycompany.travelagency.mapping.TourMapper;
import com.mycompany.travelagency.repository.TourRepository;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @class Claim
 * 
 * This class represents claim in domain model
 * 
 * 
 * @author rudolph
 */
public class Claim extends DomainObject {
    
    private String requesterName;
    private long requesterId;
    
    private String tourOwnerName;
    private long tourOwnerId;
    
    private ArrayList<Customer> customers;
    private String requestDate;
    private String tourName;
    
    private long tourId; /* need for unique identification linked tour in our service
                           not in tour Operators systems */
    
    private long tourOperatorTourId;    
    private int duration;
    private int accomodation;
    
    private boolean loadTourOwner = false;
    private boolean loadRequester = false;
    private boolean loadTourName = false;
    
    
    
    /**
     * Constructor
     * 
     */
    public Claim(){
        customers = new ArrayList<Customer>();
    }

    /**
     * Retrives requester name from database or cache (lazy load pattern)
     * 
     * @return requester name 
     */
    public String getRequesterName() {
        
        if(!loadRequester){
            CompanyMapper mapper = CompanyMapper.getMapper();
            Company company = mapper.find(requesterId);
            requesterName = company.getName();
        }
        loadRequester = true;
        return requesterName;
    }

    public void setRequesterName(String requesterName) {
        this.requesterName = requesterName;
    }

    /**
     * Retrives tour owner name
     * 
     * @return tour owner name 
     */
    public String getTourOwnerName() {
                
        if(!loadTourOwner){
            CompanyMapper mapper = CompanyMapper.getMapper();
            Company company = mapper.find(tourOwnerId);
            tourOwnerName = company.getName();
        }
        loadTourOwner = true;
        return tourOwnerName;
    }

    public void setTourOwnerName(String tourOwnerName) {
        this.tourOwnerName = tourOwnerName;
    }

    /**
     * Retrives list of customers
     * 
     * @return list of customers 
     */
    public ArrayList<Customer> getCustomers() {
        return customers;
    }
    
    /**
     * Checks if the customer is in customers list
     * 
     * @param customer Customer instance to check 
     * @return true if customer is in customers list, false otherwise
     */
    public boolean hasCustomer(Customer customer){
        return customers.contains(customer);
    }
    
    /**
     * Adds customer to list of customers if it not stored there yet
     * 
     * @param customer Customer instance to add to list
     */
    public void addCustomer(Customer customer){
        if(!this.hasCustomer(customer)){
            customers.add(customer);
        }
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
    
   
    
    /**
     * 
     * Method to get Date as String
     * (format YYYY-MM-DD not controlled)
     * 
     * @return string that represents date
     * 
     */
    public String getRequestDate(){
        return requestDate;
    }
    
    
    
    /**
    *
    * Method to set requestDate from String
    * (format YYYY-MM-DD not controlled)
    * 
    * @return string that represents date
    * 
    */ 
    public void setRequestDate(String date){
                
        requestDate = date;        
    }

    /**
     * Retrives tour name from database or cache 
     * 
     * @return tour name 
     */
    public String getTourName() {
        
        if(!loadTourName){
            TourRepository repository = new TourRepository();
            //Tour tour = repository.findTourById(requesterId, tourId);
            Tour tour = repository.findTourByIdAsUser(tourId);
            tourName = tour.getOptionalAttribute("name");
        }
        loadTourName = true;
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }
    
    /**
     * Make and return short customers info
     * For example: Ivanov Ivan Ivanovich mapped to Ivanov I.I.
     * Another example: Sergeev Sergey mapped to Sergeev S.
     * Yet another example: Петров Петр Петрович, Смирнов Сергей mapped to Петров П.П., Смирнов С. 
     * 
     * @return short description of customers
     */
    public String getShortCustomersInfo(){
        
        StringBuilder builder = new StringBuilder();
        
        boolean firstCustomer = true;
        for(Customer customer : customers){
           if( firstCustomer ){
               firstCustomer = false;
           }
           else {
               builder.append(", ");
           }    
           String transName = customer.getRuName();
           if(transName.equals(Customer.DEFAULT_MSG) || transName.isEmpty() ){
               transName = customer.getTransborderName();
           }
           
           
            String[] nameParts = transName.split(" ");
            boolean firstPart = true;
            for( String part : nameParts ){
                 if(firstPart){
                     builder.append(part);
                     builder.append(" ");
                     firstPart = false;
                 }
                 else {
                     if (part.matches("^[a-zA-Zа-яА-Я]+")){
                         String initial = part.substring(0, 1).concat(".");
                         builder.append(initial);
                     }
                 }
            }
                     
           
        }
        
        return builder.toString();
        
    }

    public long getTourId() {
        return tourId;
    }

    public void setTourId(long tourId) {
        this.tourId = tourId;
    }

    public long getRequesterId() {
        return requesterId;
    }

    public void setRequesterId(long requesterId) {
        this.requesterId = requesterId;
    }

    public long getTourOwnerId() {
        return tourOwnerId;
    }

    public void setTourOwnerId(long tourOwnerId) {
        this.tourOwnerId = tourOwnerId;
    }

    public long getTourOperatorTourId() {
        return tourOperatorTourId;
    }

    public void setTourOperatorTourId(long tourOperatorTourId) {
        this.tourOperatorTourId = tourOperatorTourId;
    }

    public int getAccomodation() {
        return accomodation;
    }

    public void setAccomodation(int accomodation) {
        this.accomodation = accomodation;
    }

    
    
}
