/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.repository;

import java.nio.charset.Charset;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * @class Criteria
 * 
 * This class represents complex criteria for query to the database
 * 
 * @author rudolph
 */
public class Criteria {
    
    // TODO JSON Structures
    JSONObject rootObject = new JSONObject();
    
    /**
     * Constructor
     * 
     */
    public Criteria(){        
    }
    
    /*
    public void addCriteria(String name, Object value){
    
        rootObject.put(name, value);
    }
    */
    
    /**
     * Adds criteria
     * 
     * @param name criteria name
     * @param value criteria value 
     */
    public void addCriteria(String name, Long value){
        
        rootObject.put(name, value);
    }
    
    /**
     * Adds criteria
     * 
     * @param name criteria name
     * @param value criteria value 
     */
    public void addCriteria(String name, Integer value){
        
        rootObject.put(name, value);
    }
    
    /**
     * Adds criteria
     * 
     * @param name criteria name
     * @param value criteria value 
     */
    public void addCriteria(String name, String value){
        
        rootObject.put(name, value);
    }
    
    /**
     * Adds array of criterias
     * 
     * @param name criteria array name
     * @param arr json array
     */
    public void addCriteriaArray(String name, JSONArray arr){
        rootObject.put(name, arr);
    }
    
    /**
     * Adds criteria
     * 
     * @param name criteria name
     * @param criteria another criteria to set
     */
    public void addCriteria(String name,Criteria criteria){
        rootObject.put(name,criteria.toJsonObject());
    }
    
    /**
     * Retrives complex criteria as JSONObject instance
     * 
     * @return JSONObject instance 
     */
    public JSONObject toJsonObject(){
        return rootObject;
    }

    @Override
    public String toString() {
        return rootObject.toString();
    }
    
    
    
}
