/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.repository;

import com.mycompany.travelagency.logic.Company;
import com.mycompany.travelagency.logic.CompanyCreator;
import com.mycompany.travelagency.logic.CompanyType;
import com.mycompany.travelagency.logic.DomainObject;
import com.mycompany.travelagency.logic.TourOperator;
import com.mycompany.travelagency.logic.TravelAgency;
import com.mycompany.travelagency.logic.User;
import com.mycompany.travelagency.mapping.CompanyMapper;
import com.mycompany.travelagency.mapping.Mappers;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import utils.Utils;

/**
 * @class Company repository
 * 
 * @author rudolph
 */
public class CompanyRepository extends Repository {

    /**
     * Constructor
     * 
     */
    public CompanyRepository(){        
    }
    
    /**
     * Rettrives list of companies with selected companyType
     * 
     * @param companyType type of company 
     * @return list of companies with selected companyType
     */    
    public List<Company> findCompanyByType(CompanyType companyType){
         
         Criteria criteria = new Criteria();
         criteria.addCriteria("messageType", "FindCompany");
         criteria.addCriteria("requesterId", Utils.getUserIdAsLong());  
         JSONArray array = new JSONArray();
         
         if(companyType == CompanyType.ALL ){
             array.add(CompanyType.OPERATOR.getIntValue());
             array.add(CompanyType.AGENCY.getIntValue());  
         }
         else {
             array.add(companyType.getIntValue());
         }
                      
         criteria.addCriteriaArray("types", array);
         
         return (List<Company>) this.matching(Company.class, criteria);
     }
     
     /**
      * Retrives current company
      * 
      * @return current logged Company instance 
      */
     public Company findCurrentCompany(){
                  
         return this.findCompanyById(Utils.getUserIdAsLong());         
     }
     
     /**
      * Retrives company by id 
      * 
      * @param id id of company to find
      * @return company with id 
      */
     public Company findCompanyById(Long id){
         
         Criteria criteria = new Criteria();
         criteria.addCriteria("messageType", "GetCompany");
         criteria.addCriteria("requesterId", Utils.getUserIdAsLong());
         criteria.addCriteria("companyId", id);
        
         User user = new User();
         user.setLogin(Utils.getUserIdAsStr());
         user.setPassword(Utils.getPasswdAsStr());
         return mapToCompany( matchingData(user, criteria) );
     }
     
     /**
      * Maps string data to Company instance
      * 
      * @param data string data to map to Company
      * @return Company instance
      */
     private Company mapToCompany(String data){
         
         CompanyMapper mapper = CompanyMapper.getMapper();
         return mapper.createObject(data);
     }
     
    /**
     * Retrives all companies
     * 
     * @return list of all available companies
     */
     public List<Company> findAllCompanies(){
         return this.findCompanyByType(CompanyType.ALL);
     }
     
     /**
      * Retrives all available tour operators 
      * 
      * @return list of all available tour operators 
      */
     public List<Company> findTourOperators(){
         return this.findCompanyByType(CompanyType.OPERATOR);
     }
     
     /**
      * Retrives all available travel agencies
      * 
      * @return  list of all available travel agencies
      */
     public List<Company> findTravelAgencies(){
         return this.findCompanyByType(CompanyType.AGENCY);
     }
     
     /**
      * Retrives list of attendant tour operators
      * 
      * @param agency selected agency for finding attendant operators
      * @return list of attendant tour operators
      */
     public List<TourOperator> findAttendantOperators(TravelAgency agency){
         
         Criteria criteria = new Criteria();
         criteria.addCriteria("messageType", "FindSubscriptions");
         criteria.addCriteria("requesterId", agency.getId());
         
         User user = new User();         
         user.setLogin(String.valueOf(agency.getId()));
         user.setPassword(String.valueOf(agency.getId()));
         
         List<TourOperator> attendants;

         attendants = findAttendantOperators(
                                getAttendantsIds(
                                    matchingData(user, criteria)));
         agency.setAttendantOperators((ArrayList<TourOperator>) attendants);
         
         return attendants; 
         
         
     }
     
     /**
      * Retrives list of attendant operators ids from dataSource string
      * 
      * 
      * @param dataSource string data to parse
      * @return list of attendant operators ids
      */
     private ArrayList<Integer> getAttendantsIds(String dataSource){
         ArrayList<Integer> attendantIds = new ArrayList<Integer>();
         JSONParser parser = new JSONParser();          
         
         try {
            
               JSONObject object = (JSONObject) parser.parse(dataSource);
               JSONArray jArray = (JSONArray) object.get("findSubscriptionsResult");
               if(jArray != null){
                   
                   System.out.println("IS FOUND SUBSCRIPTION");                   
                   System.out.println("SUBSCRIPT JARRAY SIZE = " + jArray.size());

                   for(int i = 0; i < jArray.size(); i++){
                                              
                       int id = Integer.parseInt(jArray.get(i).toString());
                       attendantIds.add(id);
                   }

               }
                         
            System.out.println("END WHILE SUBSCR");
            
         } catch (ParseException ex) {
            Logger.getLogger(CompanyMapper.class.getName()).
                   log(Level.SEVERE, "Parse Exceptions occurred", ex);
         }
         
         return attendantIds;
         
     }
     
     
     /**
      * Retrives list of attendant tour operators
      * 
      * @param ids list of attendant operators ids
      * @return list of attendant tour operators
      */
     private List<TourOperator> findAttendantOperators(ArrayList<Integer> ids){
         List<TourOperator> attendants = new ArrayList();
         CompanyMapper mapper = CompanyMapper.getMapper();
         Iterator<Integer> it = ids.iterator();
         for( ; it.hasNext() ; ){
             Long id = it.next().longValue();
             if(mapper.isLoadedDomainObject(id)){
                 
                System.out.println("OPERATOR ID = " + id + " DOMAIN OBJ =  "
                        + mapper.loadDomainObject(id).toString());
                Company company = mapper.loadDomainObject(id);
                if(company.getClass() == TourOperator.class){
                    attendants.add((TourOperator) company);
                }
                it.remove();
             }
         }
         
         if(ids.isEmpty()){
             return attendants;
         }            
                  
         for( Integer id : ids ){
             long operatorId = id.longValue();
             TourOperator operator = (TourOperator) this.findCompanyById(operatorId);
             if(operator != null){
                 attendants.add(operator);
             }
         }
               
         
         return attendants;
     }
     
     /**
      * Adds subscription of travel agency for tour operators tours
      * 
      * 
      * @param travelAgencyId id of travel agency
      * @param tourOperatorId id of tour operator
      */
     public void addSubscription(Long travelAgencyId, Long tourOperatorId ){
         
         Criteria criteria = new Criteria();
         criteria.addCriteria("messageType", "Subscribe");
         criteria.addCriteria("requesterId", travelAgencyId);
         JSONArray array = new JSONArray();
         array.add(tourOperatorId);
         criteria.addCriteriaArray("ids", array);
         
         User user = new User();
         user.setLogin(String.valueOf(travelAgencyId));
         user.setPassword(String.valueOf(travelAgencyId));
         this.matchingData(user, criteria);
         
     }
     
     /**
      * Removes subscription of travel agency for tour operators tours
      * 
      * @param travelAgencyId id of travel agency
      * @param tourOperatorId of tour operator
      */
     public void removeSubscription(Long travelAgencyId, Long tourOperatorId){
         
         Criteria criteria = new Criteria();
         criteria.addCriteria("messageType", "Unsubscribe");
         criteria.addCriteria("requesterId", travelAgencyId);
         JSONArray array = new JSONArray();
         array.add(tourOperatorId);
         criteria.addCriteriaArray("ids", array);
         
         User user = new User();
         user.setLogin(String.valueOf(travelAgencyId));
         user.setPassword(String.valueOf(travelAgencyId));
         this.matchingData(user, criteria);
     }
     
     
}
