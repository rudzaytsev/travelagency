/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.repository;

import com.mycompany.travelagency.logic.Tour;
import com.mycompany.travelagency.logic.TourAddParams;
import com.mycompany.travelagency.logic.TourSearchParams;
import com.mycompany.travelagency.logic.TourType;
import com.mycompany.travelagency.logic.User;
import com.mycompany.travelagency.mapping.TourMapper;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import utils.TwoTuple;
import utils.Utils;

/**
 * @class TourRepository
 * 
 * 
 * @author rudolph
 */
public class TourRepository extends Repository {
    
    /**
     * Finds Tour By tourId as subscribedCompanyId 
     * 
     * 
     * @param subscribedCompanyId
     * @param tourId id of tour to find
     * @return Tour instance with tourId
     */
    public Tour findTourById(Long subscribedCompanyId ,Long tourId){
        
         Criteria criteria = new Criteria();
         criteria.addCriteria("messageType", "GetTour");
         criteria.addCriteria("requesterId", subscribedCompanyId );
         criteria.addCriteria("tourId", tourId);
         
         User user = new User();
         user.setLogin(String.valueOf(subscribedCompanyId));
         user.setPassword(String.valueOf(subscribedCompanyId));
              
         return mapToTour(this.matchingData(user, criteria));
        
    }
    
    /**
     * Finds tour by id as current Company requester
     * 
     * 
     * @param id  id of tour to find
     * @return Tour instance with concrete id 
     */
    public Tour findTourByIdAsUser(Long id){
        
         Criteria criteria = new Criteria();
         criteria.addCriteria("messageType", "GetTour");
         criteria.addCriteria("requesterId", Utils.getUserIdAsLong() );
         criteria.addCriteria("tourId", id);
         
         User user = new User();
         user.setLogin(Utils.getUserIdAsStr());
         user.setPassword(Utils.getPasswdAsStr());
         
         return  mapToTour(this.matchingData(user, criteria));
        
    }
    
    /**
     * Retrives Tour instance from string dataSource
     * 
     * 
     * @param dataSource string data to parse
     * @return Tour instance
     */
    private Tour mapToTour(String dataSource){
        
        TourMapper mapper = TourMapper.getMapper();
        return mapper.createObject(dataSource);
    }
    
    /**
     * Retrives list of tours from string data 
     * 
     * @param dataSource string data to parse
     * @return list of tours
     */
    private List<Tour> mapToTourList(String dataSource){
        
        TourMapper mapper = TourMapper.getMapper();
        return mapper.createAll(dataSource);
    }
    
    /**
     * Finds tours by type
     * 
     * @param type type of tour
     * @return list of tours with selected type
     */
    public List<Tour> findToursByType(TourType type){
        
        Criteria criteria = new Criteria();
        criteria.addCriteria("messageType", "FindTours");
        criteria.addCriteria("requesterId", Utils.getUserIdAsLong());
        criteria.addCriteria("offset", 0l);
        criteria.addCriteria("limit", 1000l);
        
        JSONArray array = new JSONArray();
        JSONObject typeCriteria;
        
        typeCriteria = this.getTypeCriteria(type);
        if(typeCriteria != null){
            array.add(typeCriteria);
        
            criteria.addCriteriaArray("findParams", array);
        }
        else {
           
            array.add(this.getTypeCriteria(TourType.TOUR));
            criteria.addCriteriaArray("findParams", array);
        }
        
        User user = new User();
        user.setLogin(Utils.getUserIdAsStr());
        user.setPassword(Utils.getPasswdAsStr());
        
        return mapToTourList(matchingData(user, criteria));
    }
    
    /**
     * Retrives all tours and hotels
     * 
     * @return List of tours and hotels
     */    
    public List<Tour> findAll(){        
        List<Tour> tours = findToursByType(TourType.TOUR);
        List<Tour> hotels = findToursByType(TourType.HOTEL);
        tours.addAll(hotels);
        return sortTours(tours);        
    }
    
    /**
     * Retrives sorted list of all tours
     * 
     * @return sorted list of all tours
     */
    public List<Tour> findTours(){
        return sortTours(findToursByType(TourType.TOUR));
    }
    
    /**
     * 
     * Retrives sorted list of all hotels
     * 
     * @return  sorted list of all hotels
     */
    public List<Tour> findHotels(){
        return sortTours(findToursByType(TourType.HOTEL));
    }
    
    /**
     * Retrives list of tours by parameters
     * 
     * 
     * @param params parameters for searching tours
     * @return list that matches parameters
     */
    public List<Tour> findByParams(TourSearchParams params){
                
        Criteria criteria = new Criteria();
        criteria.addCriteria("messageType", "FindTours");
        criteria.addCriteria("requesterId", Utils.getUserIdAsLong());
        criteria.addCriteria("offset", 0l);
        criteria.addCriteria("limit", 1000l);        
        
        JSONArray findParams = new JSONArray();
       
        TourType type = params.getSelectedTourType();
        
        JSONObject typeCriteria = this.getTypeCriteria(type);
        if(typeCriteria != null){
            findParams.add(typeCriteria);     
        }
        
        String country = params.getSelectedCountry();
        if(!country.isEmpty()){
            findParams.add(this.getCountryCriteria(country));
        }
         
        String town = params.getSelectedTown();
        if(!town.isEmpty()){
            findParams.add(this.getTownCriteria(town));
        }
        
        String hotel = params.getSelectedHotel();
        if(!hotel.isEmpty()){
            findParams.add(this.getHotelCriteria(hotel));
        }
        
        String priceAmount = params.getSelectedPriceAmount();
        if(!priceAmount.isEmpty()){
            String amountCondition = params.getSelectedAmountCondition();            
            findParams.add(this.getAmountCriteria(priceAmount, amountCondition));
        }
        
        String priceCurrency = params.getSelectedPriceCurrency();
        if(!priceCurrency.isEmpty()){
            findParams.add(this.getCurrencyCriteria(priceCurrency));
        }
        
        String priceUnit = params.getSelectedPriceUnit();
        if(!priceUnit.isEmpty()){
            findParams.add(this.getUnitCriteria(priceUnit));
        }
        
        String begDateLowerBound = params.getBeginDateLowerBound();
        if(!begDateLowerBound.isEmpty()){
            findParams.add(this.getBegDateLowerBoundCriteria(begDateLowerBound));
        }
        String begDateUpperBound = params.getBeginDateUpperBound();
        if(!begDateUpperBound.isEmpty()){
            findParams.add(this.getBegDateUpperBoundCriteria(begDateUpperBound));
        }
        
        String endDateLowerBound = params.getEndDateLowerBound();
        if(!endDateLowerBound.isEmpty()){
            findParams.add(this.getEndDateLowerBoundCriteria(endDateLowerBound));
        }
        String endDateUpperBound = params.getEndDateUpperBound();
        if(!endDateUpperBound.isEmpty() ){
            findParams.add(this.getEndDateUpperBoundCriteria(endDateUpperBound));
        }
        
        JSONArray orderBy;
        criteria.addCriteriaArray("findParams", findParams);
        if( (orderBy = this.getSortOrder(params)) != null){
            criteria.addCriteriaArray("orderby", orderBy);
        }
                
        User user = new User();
        user.setLogin(Utils.getUserIdAsStr());
        user.setPassword(Utils.getPasswdAsStr());        
        
        return mapToTourList(this.matchingData(user, criteria));
    }
   
   /**
    * Retrives JSONArray with sort order
    * 
    * @param params parameters for searching
    * @return JSONArray with sort order parameters
    */
   private JSONArray getSortOrder(TourSearchParams params){
       
       JSONObject orderCriteria = new JSONObject();
       if(params.getSortField() != TourSearchParams.NO_SORT){
            orderCriteria.put("field", params.getSortField());
            orderCriteria.put("condition", params.getSortDirection());
            JSONArray sortOrder = new JSONArray();
            sortOrder.add(orderCriteria);
            return sortOrder;
       }
       return null; 
       
   }
    
    private JSONObject getTypeCriteria(TourType type){
        
        JSONObject typeCriteria = new JSONObject();
                
        if(type == TourType.TOUR){
            typeCriteria.put("name", "type");
            typeCriteria.put("value","Tour");
            typeCriteria.put("condition", "eq");            
        }
        else if(type == TourType.HOTEL){
            typeCriteria.put("name", "type");
            typeCriteria.put("value","Hotel");
            typeCriteria.put("condition", "eq");            
        }
        else {
            return null;
        }
        
        return typeCriteria;        
    }
    
        
    private JSONObject getCountryCriteria(String country){
        
        JSONObject countryCriteria = new JSONObject();
        countryCriteria.put("name", "country");
        countryCriteria.put("value", country);
        countryCriteria.put("condition", "eq");
        return countryCriteria;        
    }
    
    private JSONObject getTownCriteria(String town){
        
        JSONObject townCriteria = new JSONObject();
        townCriteria.put("name", "town");
        townCriteria.put("value", town);
        townCriteria.put("condition","eq");
        return townCriteria;
    }
    
    private JSONObject getHotelCriteria(String hotel){
        
        JSONObject hotelCriteria = new JSONObject();
        hotelCriteria.put("name", "hotel");
        hotelCriteria.put("value", hotel);
        hotelCriteria.put("condition", "eq");
        return hotelCriteria;
    }
    
    private JSONObject getAmountCriteria(String amount, String amountCondition){
        JSONObject amountCriteria = new JSONObject();
        amountCriteria.put("name", "priceAmount");
        amountCriteria.put("value", Long.parseLong(amount));
        amountCriteria.put("condition", mapCondition(amountCondition));
        return amountCriteria;
    }
    
    private JSONObject getBegDateLowerBoundCriteria(String bound){
        JSONObject boundCriteria = new JSONObject();
        boundCriteria.put("name", "beginDate");
        boundCriteria.put("value", bound);
        boundCriteria.put("condition", "ge");
        return boundCriteria;
    }
    
    private JSONObject getBegDateUpperBoundCriteria(String bound){
        JSONObject boundCriteria = new JSONObject();
        boundCriteria.put("name", "beginDate");
        boundCriteria.put("value", bound);
        boundCriteria.put("condition", "le");
        return boundCriteria;
    }
    
    private JSONObject getEndDateLowerBoundCriteria(String bound){
        JSONObject boundCriteria = new JSONObject();
        boundCriteria.put("name", "endDate");
        boundCriteria.put("value", bound);
        boundCriteria.put("condition", "ge");
        return boundCriteria;
    }
    
    private JSONObject getEndDateUpperBoundCriteria(String bound){
        JSONObject boundCriteria = new JSONObject();
        boundCriteria.put("name", "endDate");
        boundCriteria.put("value", bound);
        boundCriteria.put("condition", "le");
        return boundCriteria;
    }
    
    private String mapCondition(String cond){
        if(cond.equals(TourSearchParams.AMOUNT_GREATER)) return "gt";
        if(cond.equals(TourSearchParams.AMOUNT_LESSER)) return "lt";
        if(cond.equals(TourSearchParams.AMOUNT_LESSER_EQUALS)) return "le";
        if(cond.equals(TourSearchParams.AMOUNT_GREATER_EQUALS)) return "ge";
        return "eq";        
    }
    
    private JSONObject getCurrencyCriteria(String currency){
        JSONObject currencyCriteria = new JSONObject();
        currencyCriteria.put("name", "priceCurrency");
        currencyCriteria.put("value", currency);
        currencyCriteria.put("condition", "eq");
        return currencyCriteria;
    }
    
    private JSONObject getUnitCriteria(String unit){
        JSONObject unitCriteria = new JSONObject();
        unitCriteria.put("name", "priceUnit");
        unitCriteria.put("value", unit);
        unitCriteria.put("condition", "eq");
        return unitCriteria;
    }
    
    private List<Tour> findToursByCurrentOperator(){
        
        CompanyRepository repository = new CompanyRepository();
        long currentCompanyId = repository.findCurrentCompany().getId();
                
        Criteria criteria = new Criteria();
        criteria.addCriteria("messageType", "FindTours");
        criteria.addCriteria("requesterId", Utils.getUserIdAsLong());
        criteria.addCriteria("offset", 0l);
        criteria.addCriteria("limit", 1000l);
        
        JSONArray array = new JSONArray();
        
        JSONObject companyCriteria = new JSONObject();
        companyCriteria.put("name", "tourOperatorId");
        companyCriteria.put("value", currentCompanyId);
        companyCriteria.put("condition", "eq");
        array.add(companyCriteria);
        
        JSONObject tourIdCriteria = new JSONObject();
        tourIdCriteria.put("name", "tourOperatorTourId");
        tourIdCriteria.put("value", 0l);
        tourIdCriteria.put("condition", "gt");
        array.add(tourIdCriteria);
                
        criteria.addCriteriaArray("findParams", array);
                 
        User user = new User();
        user.setLogin(Utils.getUserIdAsStr());
        user.setPassword(Utils.getPasswdAsStr());
        
        return mapToTourList(this.matchingData(user, criteria));
    }
    
    /**
    * This method generates tourOperatorTourId     
    * @return tourOperatorTourId
    */ 
    private long generateTourOperatorTourId(){
            
        return Utils.generateTourOperatorTourId();        
    }
    
    /**
     * Adds tour with parameters to database
     * 
     * 
     * @param params parameters of tour to be added
     */
    public void addTourWithParams(TourAddParams params){
        
        Criteria criteria = new Criteria();
        criteria.addCriteria("messageType", "ToursAdd");
        criteria.addCriteria("requesterId",Utils.getUserIdAsLong());
        
        JSONArray array = new JSONArray();        
        JSONObject data = new JSONObject();
        
        data.put("tourOperatorTourId", this.generateTourOperatorTourId());
        data.put("parentTourId", this.generateTourOperatorTourId());
        data.put("lastModified", Utils.getCurrentDateTime());
        data.put("name", params.getSelectedName());
        data.put("type", (params.getTourType() == TourType.TOUR) ? "Tour" : "Hotel");
        
        data.put("country", params.getSelectedCountry());
        data.put("town", params.getSelectedTown());
        data.put("hotel", params.getSelectedHotel());
        data.put("duration",params.getSelectedDuration());
        data.put("room", params.getSelectedRoom());
        data.put("accomodation", params.getSelectedAccomodation());
        data.put("food",params.getSelectedFood());
        data.put("aircompany", params.getSelectedAirCompany());
        data.put("beginDate", params.getSelectedBeginDate());
        data.put("endDate", params.getSelectedEndDate());
        data.put("description", params.getSelectedDescription());
        data.put("priceAmount", params.getSelectedPriceAmount());
        data.put("priceCurrency",params.getSelectedPriceCurrency());
        data.put("priceUnit", params.getSelectedPriceUnit());
        
        array.add(data);
        
        criteria.addCriteriaArray("requestData", array);
        
        User user = new User();
        user.setLogin(Utils.getUserIdAsStr());
        user.setPassword(Utils.getPasswdAsStr());
        
        String response = this.matchingData(user, criteria);        
        System.out.println("**** Response Add : " + response);
        
    }

    /**
     * Removes tour with id (tourOperatorTourId) from database
     * 
     * 
     * @param id tourOperatorTourId of tour to remove from database
     */
    public void deleteTourById(Long id){
        
        Criteria criteria = new Criteria();
        criteria.addCriteria("messageType", "DeleteTours");
        criteria.addCriteria("requesterId", Utils.getUserIdAsLong());
        JSONArray array = new JSONArray();
        array.add(id);
        criteria.addCriteriaArray("tourIds",array);
        
        User user = new User();
        user.setLogin(Utils.getUserIdAsStr());
        user.setPassword(Utils.getPasswdAsStr());
        
        String response = this.matchingData(user, criteria);
        System.out.println("**** Response Del : " + response);
    }
    
    /**
     * Sort list of tours by id. Sort Order is decrease
     * 
     * 
     * @param tours tours list to sort
     * @return sorted list of tours
     */
    private List<Tour> sortTours(List<Tour> tours){
        
        class TourComparator implements Comparator<Tour>{

            @Override
            public int compare(Tour o1, Tour o2) {
                if(o1.getId() > o2.getId()) return -1;
                if(o1.getId() < o2.getId()) return 1;
                return 0;
            }            
        } 
        
        Collections.sort(tours, new TourComparator());
        
        return tours;
    }
    
    
    
}
