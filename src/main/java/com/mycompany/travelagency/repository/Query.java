/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.repository;

import com.mycompany.travelagency.logic.DomainObject;
import com.mycompany.travelagency.logic.User;
import com.mycompany.travelagency.mapping.AbstractMapper;
import com.mycompany.travelagency.mapping.Mappers;
import java.util.ArrayList;
import java.util.List;
import utils.RestClient;
import utils.Utils;

/**
 * @class Query
 * 
 * 
 * @author rudolph
 */
public class Query {
    
    private Class klass = null;
    private Criteria criteria = null;
    private User user = null;
    
    /**
     * 
     * Constructor
     * 
     */
    public Query(){
        user = new User();
        user.setLogin((String) Utils.getSession().getAttribute("user"));
        user.setPassword((String) Utils.getSession().getAttribute("passwd"));
    }
    
    /**
     * Constructor
     * 
     * 
     * @param user User instance 
     */
    public Query(User user){
        this.user = user;
    }
    
    /**
     * Constructor
     * 
     * @param klass Class instance 
     */
    public Query(Class klass){
        this();
        this.klass = klass;
    }
    
    /**
     * Adds creiteria
     * 
     * @param criteria criteria to add  
     */
    public void addCriteria(Criteria criteria){
        this.criteria = criteria;
    }
    
    /**
     * Executes of query
     * 
     * @return List of Domain Objects 
     */
    public List execute(){
                               
        AbstractMapper<DomainObject> mapper = Mappers.getMapperForClass(klass);
        return mapper.createAll(this.getResponse());
       
    }
    
    
    /**
     * Retrives response for query
     * 
     * @return response for query 
     */ 
    public String getResponse(){
        
        RestClient client = new RestClient();
        
        client.setUrl(Utils.DATASOURCE_URL);
        client.setUser( user.getLogin() );
        client.setPassword(user.getPassword() );
        
        RestClient.RequestParams reqParams = client.new RequestParams();
        reqParams.setContentType("application/json");
        reqParams.setVersion("2");
        reqParams.setRequestBody(criteria.toString());
        
        client.setRequestParams(reqParams);
        
        System.out.println("User " +  client.getUser());
        System.out.println("Pass " +  client.getPassword());
        
        System.out.println("REQUEST BODY: " +  client.getRequestParams().getRequestBody());
        String content = client.postRequest();
        
        return content;
    }

    /**
     * Sets class for mapper to transform response to domain objects
     * 
     * @param klass 
     */
    public void setKlass(Class klass) {
        this.klass = klass;
    }
    
    
    
}
