/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.repository;

import com.mycompany.travelagency.logic.DomainObject;
import com.mycompany.travelagency.logic.User;
import java.util.List;

/**
 * @class Repository
 * This class represents repository
 * 
 * 
 * @author rudolph
 */
public abstract class Repository {
    
      
    /**
     * Retrives list of Domain Objects that match all criterias
     * 
     * @param entityClass class to define mapper
     * @param criterias criterias
     * @return list of Domain Objects that match all criterias
     */
    public List matching(Class<? extends DomainObject> entityClass, Criteria criterias) {
        
        Query query = new Query(entityClass);
        query.addCriteria(criterias);
        return query.execute();
        
    }
    
    /**
     * Retrives list of Domain Objects that match all criterias
     * 
     * @param user user (requester)
     * @param criterias criterias
     * @return list of Domain Objects that match all criterias
     */
    public List matchingList(User user,Criteria criterias){
        
        Query query = new Query(user);
        query.addCriteria(criterias);
        return query.execute();
    }
    
    /**
     * Retrives list of Domain Objects that match all criterias
     * 
     * @param entityClass class to define mapper
     * @param user user (requester)
     * @param criterias criterias
     * @return list of Domain Objects that match all criterias
     */
    public List matchingList(Class<? extends DomainObject> entityClass, User user, Criteria criterias){
        Query query = new Query(user);
        query.addCriteria(criterias);
        query.setKlass(entityClass);
        return query.execute();   
    }
    
    /**
     * Retrives Object that match all criterias
     * 
     * 
     * @param entityClass class to define mapper
     * @param criterias criterias
     * @return object that match all criterias
     */
    public Object matchingObject(Class<? extends DomainObject> entityClass, Criteria criterias){
        List list = matching( entityClass, criterias);
        return  list.isEmpty() ? null : list.get(0) ;
    }
    
    
    /**
     * Retrives Object that match all criterias
     * 
     * @param user  user (requester)
     * @param criterias criterias
     * @return object that match all criterias
     */
    public Object matchingObject(User user, Criteria criterias ){
        List list = matchingList(user, criterias);
        return list.isEmpty() ? null : list.get(0);
    }
   
    /**
     * Retrives raw response data
     * 
     * @param user user (requester)
     * @param criterias criterias
     * @return raw string response data
     */
    public String matchingData(User user, Criteria criterias){
        
        Query query = new Query(user);
        query.addCriteria(criterias);
        return query.getResponse();
    }
    
    
    
    
}
