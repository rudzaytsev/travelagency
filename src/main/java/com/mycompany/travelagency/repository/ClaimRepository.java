/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.travelagency.repository;

import com.mycompany.travelagency.logic.Claim;
import com.mycompany.travelagency.logic.ClaimDirection;
import com.mycompany.travelagency.logic.Company;
import com.mycompany.travelagency.logic.Customer;
import com.mycompany.travelagency.logic.TravelAgency;
import com.mycompany.travelagency.logic.User;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import utils.Utils;
import view.ClaimAddParams;
import view.CustomerAdapter;

/**
 * @class ClaimRepository
 * 
 * 
 * @author rudolph
 */
public class ClaimRepository extends Repository {
    
    
    /**
     * Finds list of claims by direction
     * 
     * @param direction ClaimDirection instance
     * @param companyId company id
     * @return list of claims 
     */
    public List<Claim> findClaimsByDirection(ClaimDirection direction, Long companyId ){
        
        Criteria criteria = new Criteria();
        
        if(ClaimDirection.FOR_OPERATOR == direction){
            criteria.addCriteria("messageType", "ClaimFindByTourOwner");            
        }
        else { // ClaimDirection.FROM_AGENCY == direction
            criteria.addCriteria("messageType", "ClaimFindByRequester");         
        }
        
        criteria.addCriteria("requesterId", companyId);
        criteria.addCriteria("findParams",companyId);
        User user = new User();
        user.setLogin(Utils.getUserIdAsStr() );
        user.setPassword(Utils.getPasswdAsStr());
        
        List<Claim> resultClaims = ( List<Claim> ) this.matchingList(Claim.class,user, criteria);
        sortClaims(resultClaims);
        
        return resultClaims;
        
    }
     
    
    /**
     * Finds list of claims by operator id
     * 
     * 
     * @param operatorId id of operator
     * @return list of claims for operator with operatorId
     */
    public List<Claim> findClaimsForOperator(Long operatorId){
        return this.findClaimsByDirection(ClaimDirection.FOR_OPERATOR, operatorId);
    }
    
    /**
     * Finds list of claims by agency id
     * 
     * 
     * @param agencyId id of agency
     * @return list of claims for agency with agency id is agencyId
     */
    public List<Claim> findClaimsFromAgency(Long agencyId){
        return this.findClaimsByDirection(ClaimDirection.FROM_AGENCY, agencyId);
    }
    
    /**
     * Finds  claims for current company
     * 
     * @param company company to find claims for
     * @return list of claims for company
     */
    public List<Claim> findClaimsByCompany(Company company){
        
        System.out.println(" findClaimsByCompany INVOKED !!!!");
        System.out.println("Company id = " + company.getId());
               
        ClaimRepository claimRepository = new ClaimRepository();
        List<Claim> claims = null;
        if(company instanceof TravelAgency){
            claims = claimRepository.findClaimsFromAgency(company.getId());
            System.out.println(" TravelAgency.class BRANCH INVOKED");
        }
        else { //TourOperator.class == company.getClass
            claims = claimRepository.findClaimsForOperator(company.getId());
            System.out.println(" TourOperator.class BRANCH INVOKED");
        }
        
        sortClaims(claims);
        
        return claims;
    }
    
    /**
     * Sorts claims by claim id. Sort order is decrease.
     * 
     * @param claims list to be sorted by claim id 
     */
    private void sortClaims(List<Claim> claims){
        
       class ClaimComparator implements Comparator<Claim> {

            @Override
            public int compare(Claim o1, Claim o2) {
                if( o1.getId() < o2.getId() ) return 1;
                if( o1.getId() > o2.getId() ) return -1;
                return 0;
            }
        
        } 
        
        Collections.sort(claims, new ClaimComparator());
        
    }
    
    /**
     * Finds claim by id
     * 
     * @param claimId id of claim to find
     * @return claim with claimId if found claim or null if not found claim
     */
    public Claim findClaimById(Long claimId){
        
        CompanyRepository companyRepository = new CompanyRepository();
        Company currentCompany = companyRepository.findCurrentCompany();
                
        List<Claim> allClaims = this.findClaimsByCompany(currentCompany);
        
        for(Claim claim : allClaims){            
            if(claim.getId().equals(claimId)){                
                return claim;
            }
        }
        
        return null;
    }
  
    /**
     * Adds claim to database
     * 
     * @param params instance of ClaimAddParams parameters for claim addition
     */
    public void addClaim(ClaimAddParams params){
        
        Criteria criteria = new Criteria();
        criteria.addCriteria("messageType", "ClaimAdd");
        criteria.addCriteria("requesterId", Utils.getUserIdAsLong());
        
        if(params.isSelfClaim()){
            criteria.addCriteria("recipientId",  Utils.getUserIdAsLong());
        }
        else {
            criteria.addCriteria("recipientId", params.getSelectedOperatorId());
        }
       
        criteria.addCriteria("tourId", params.getSelectedTourId());
        criteria.addCriteria("requestDate", Utils.getCurrentDate());
        criteria.addCriteria("startDate", params.getStartDate());
        criteria.addCriteria("accomodation", params.getAccomodation());
        criteria.addCriteria("duration", params.getDuration());                
        criteria.addCriteriaArray("customerData", this.addCustomers(params));
        
        User user = new User();
        user.setLogin(Utils.getUserIdAsStr() );
        user.setPassword(Utils.getPasswdAsStr());        
        
        this.matchingData(user, criteria);
    }
    
    /**
     * Builds json array with customers data
     * 
     * @param params ClaimAddParams instance, parameters for customers addition 
     * @return JSONArray of customers data
     */
    private JSONArray addCustomers(ClaimAddParams params){
        
        JSONArray array = new JSONArray();
        List<Customer> validCustomers = new ArrayList<Customer>();
        List<CustomerAdapter> adapters = params.getCustomersWrapper().getCustomersAsList();
        
        for(CustomerAdapter adapter : adapters){
            Customer customer = adapter.getAdaptedCustomer();
            if(customer.isValid()){
                validCustomers.add(customer);
            }
        }
        
        for(Customer validCustomer : validCustomers ){
            JSONObject cust = new JSONObject();
            cust.put("ruName", validCustomer.getRuName());
            cust.put("sex", validCustomer.getSex());
            cust.put("phone", validCustomer.getPhone());
            cust.put("birthDate", validCustomer.getBirthDate());
            cust.put("transborderName", validCustomer.getTransborderName());
            cust.put("transborderPassport", validCustomer.getTransborderPassport());
            cust.put("transborderFromDate", validCustomer.getTransborderFromDate());
            cust.put("transborderToDate", validCustomer.getTransborderToDate());
            array.add(cust);
        }
        
        return array;
    }
    
    /**
     * Removes claim by id from database
     * 
     * @param id of claim to remove
     */
    public void deleteClaim(Long id){
        
        Criteria criteria = new Criteria();
        criteria.addCriteria("messageType", "ClaimDelete");
        criteria.addCriteria("requesterId", Utils.getUserIdAsLong());
        criteria.addCriteria("claimId", id);
        
        User user = new User();
        user.setLogin(Utils.getUserIdAsStr() );
        user.setPassword(Utils.getPasswdAsStr());        
        
        this.matchingData(user, criteria);        
    }
    
}
